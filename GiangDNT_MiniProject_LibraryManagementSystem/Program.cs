﻿namespace GiangDNT_MiniProject_LibraryManagementSystem
{
    public class Program
    {
        static Library lib = new Library();

        static void Main(string[] args)
        {            
            int option = 0;
            do
            {
                try
                {
                    Console.Clear();

                    lib.MainMenu();
                    Console.Write("Choose your option: ");
                    if (!int.TryParse(Console.ReadLine(), out option))
                        throw new Exception("Invalid input");
                    switch (option)
                    {
                        case 1:
                            lib.DisplayBooks();
                            break;
                        case 2:
                            lib.AddBook();
                            break;
                        case 3:
                            lib.FilterBooks();
                            break;
                        case 4:
                            lib.DisplayBorrowBook();
                            break;
                        case 5:
                            lib.ManageReaders();
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (option != 6);

            lib.WhenExit();
        }        
    }
}