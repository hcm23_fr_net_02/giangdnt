﻿namespace GiangDNT_MiniProject_LibraryManagementSystem
{
    public class Constant
    {
        public const string BOOK_PATH = "..\\..\\..\\json_files\\book.json";
        public const string READER_PATH = "..\\..\\..\\json_files\\reader.json";
        public const string REVIEW_PATH = "..\\..\\..\\json_files\\review.json";
        public const string BORROWBOOK_PATH = "..\\..\\..\\json_files\\borrowbook.json";

        public const int NUM_ROW_PAGE = 10;
    }
}
