﻿namespace GiangDNT_MiniProject_LibraryManagementSystem.Models
{
    public class Review
    {
        private int _reviewId;
        public int ReviewId { get => _reviewId; set => _reviewId = value; }

        private string _title;
        public string Title { get => _title; set => _title = value; }

        private string _content;
        public string Content { get => _content; set => _content = value; }

        private int _rankStar;
        public int RankStar { get => _rankStar; set => _rankStar = value; }

        private int _bookId;
        public int BookId { get => _bookId; set => _bookId = value; }

        private int _readerId;
        public int ReaderId { get => _readerId;set=> _readerId = value; }

        public Book Book = new Book();
        public Reader Reader = new Reader();

        public Review() { }
        public Review(int reviewId, string title, string content, int rankStar, int bookId, int readerId)
        {
            ReviewId = reviewId;
            Title = title;
            Content = content;
            RankStar = rankStar;
            BookId = bookId;
            ReaderId = readerId;
        }
        public Review(int reviewId, string title, string content, int rankStar, int bookId, int readerId, Book book, Reader reader)
        {
            ReviewId = reviewId;
            Title = title;
            Content = content;
            RankStar = rankStar;
            BookId = bookId;
            ReaderId = readerId;
            Book = book;
            Reader = reader;
        }
    }
}
