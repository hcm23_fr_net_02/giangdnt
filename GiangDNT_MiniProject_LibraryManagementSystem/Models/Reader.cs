﻿namespace GiangDNT_MiniProject_LibraryManagementSystem.Models
{
    public class Reader
    {
        private int _readerId;
        public int ReaderId { get => _readerId; set => _readerId = value; }

        private string _fullName;
        public string FullName { get => _fullName; set => _fullName = value; }

        private int _age;
        public int Age { get => _age; set => _age = value; }

        private DateTime _birthDate;
        public DateTime BirthDate { get => _birthDate; set => _birthDate = value; }

        public Reader() { }
        public Reader(int readerId, string fullName, int age, DateTime birthDate)
        {
            ReaderId = readerId;
            FullName = fullName;
            Age = age;
            BirthDate = birthDate;
        }
    }
}
