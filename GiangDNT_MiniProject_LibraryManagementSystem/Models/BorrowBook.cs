﻿namespace GiangDNT_MiniProject_LibraryManagementSystem.Models
{
    public class BorrowBook
    {
        private int _id;
        public int Id { get => _id; set => _id = value; }

        private DateTime _borrowDate;
        public DateTime BorrowDate { get => _borrowDate;set => _borrowDate = value; }

        private DateTime _returnDate;
        public DateTime ReturnDate { get => _returnDate; set => _returnDate = value; }

        private int _bookId;
        public int BookId { get => _bookId; set => _bookId = value; }

        private int _readerId;
        public int ReaderId { get => _readerId; set => _readerId = value; }

        public Book Book = new Book();
        public Reader Reader = new Reader();

        public BorrowBook() { }
        public BorrowBook(int id, DateTime borrowDate, DateTime returnDate, int bookId, int readerId)
        {
            Id = id;
            BorrowDate = borrowDate;
            ReturnDate = returnDate;
            BookId = bookId;
            ReaderId = readerId;
        }
        public BorrowBook(int id, DateTime borrowDate, DateTime returnDate, int bookId, int readerId, Book book, Reader reader)
        {
            Id = id;
            BorrowDate = borrowDate;
            ReturnDate = returnDate;
            BookId = bookId;
            ReaderId = readerId;
            Book = book;
            Reader = reader;
        }
    }
}
