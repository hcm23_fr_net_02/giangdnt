﻿namespace GiangDNT_MiniProject_LibraryManagementSystem.Models
{
    public class Book
    {
        private int _bookId;
        public int BookId { get => _bookId; set => _bookId = value; }

        private string _title;
        public string Title { get => _title; set => _title = value; }

        private string _author;
        public string Author { get => _author; set => _author = value; }

        private string _publicationYear;
        public string PublicationYear { get => _publicationYear; set => _publicationYear = value; }

        private int _quantity;
        public int Quantity { get => _quantity; set => _quantity = value; }

        private string _genre;
        public string Genre { get => _genre; set => _genre = value; }

        public Book() { }
        public Book(int bookId, string title, string author, string publicationYear, int quantity, string genre)
        {
            BookId = bookId;
            Title = title;
            Author = author;
            PublicationYear = publicationYear;
            Quantity = quantity;
            Genre = genre;
        }
    }
}
