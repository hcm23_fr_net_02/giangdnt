﻿using GiangDNT_MiniProject_LibraryManagementSystem.Models;
using System.Globalization;

namespace GiangDNT_MiniProject_LibraryManagementSystem
{
    public class Library
    {
        IUnitOfWork unitOfWork;

        public Library()
        {
            unitOfWork = new UnitOfWork();
        }

        public void WhenExit()
        {
            JsonHandler<Book>.WriteArrayJson(Constant.BOOK_PATH, unitOfWork.BookRepository.GetAll());
            JsonHandler<Reader>.WriteArrayJson(Constant.READER_PATH, unitOfWork.ReaderRepository.GetAll());
            JsonHandler<Review>.WriteArrayJson(Constant.REVIEW_PATH, unitOfWork.ReviewRepository.GetAll());
            JsonHandler<BorrowBook>.WriteArrayJson(Constant.BORROWBOOK_PATH, unitOfWork.BorrowBookRepository.GetAll());
        }

        public void MainMenu()
        {
            Console.WriteLine("*****************LIBRARY MANAGEMENT SYSTEM*****************");
            Console.WriteLine("1. View books in library");
            Console.WriteLine("2. Add new book");
            Console.WriteLine("3. Filter books");
            Console.WriteLine("4. View borrowed books");
            Console.WriteLine("5. Manage reader");
            Console.WriteLine("6. Exit");
            Console.WriteLine();
        }

        public void ViewBookMenu()
        {
            Console.WriteLine("\"N\" to view next page");
            Console.WriteLine("\"P\" to view previous page");
            Console.WriteLine("\"B\" to back to main menu");
            Console.WriteLine("Enter ID of a book to view detail info");
            Console.WriteLine();
        }

        public void DetailBookMenu()
        {
            Console.WriteLine("1. Edit book info");
            Console.WriteLine("2. Delete book");
            Console.WriteLine("3. Add review");
            Console.WriteLine("4. View all book's reviews");
            Console.WriteLine("5. Borrow book");
            Console.WriteLine("6. Exit");
            Console.WriteLine();
        }

        public void FilterBooksMenu()
        {
            Console.WriteLine("1. By book name");
            Console.WriteLine("2. By book author");
            Console.WriteLine("3. By book publication year");
            Console.WriteLine("4. By book genre");
            Console.WriteLine("5. Exit");
            Console.WriteLine();
        }

        public void BorrowBookMenu()
        {
            Console.WriteLine("\"N\" to view next page");
            Console.WriteLine("\"P\" to view previous page");
            Console.WriteLine("\"B\" to back to main menu");
            Console.WriteLine("Find borrow books by entering book name or reader name. Enter ID of a borrow book to return that book");
            Console.WriteLine();
        }

        public void ManageReadersMenu()
        {
            Console.WriteLine("1. Add new reader");
            Console.WriteLine("2. View all readers");
            Console.WriteLine("3. Edit reader info");
            Console.WriteLine("4. Delete a reader");
            Console.WriteLine("5. Exit");
        }

        public void ViewReaderMenu()
        {
            Console.WriteLine("\"N\" to view next page");
            Console.WriteLine("\"P\" to view previous page");
            Console.WriteLine("\"B\" to back to previous main menu");
            Console.WriteLine();
        }

        public bool InputAndValidateIntField(string fieldName, out int field)
        {
            Console.Write($"{fieldName}");

            if(!int.TryParse(Console.ReadLine(), out field))
            {
                return false;
            }

            return true;
        }

        public bool InputAndValidateUnsignedIntField(string fieldName, out int field)
        {
            Console.Write($"{fieldName}");

            if (!int.TryParse(Console.ReadLine(), out field))
            {
                return false;
            }

            if(field < 0)
            {
                return false;
            }

            return true;
        }

        public bool InputAndValidateStringField(string fieldName, out string field)
        {
            Console.Write($"{fieldName}");
            field = Console.ReadLine();

            if (string.IsNullOrEmpty(field))
            {
                return false;
            }

            return true;
        }

        public bool InputAndValidateDateTimeField(string fieldName, out DateTime field)
        {
            Console.Write($"{fieldName}");

            if(!DateTime.TryParseExact(Console.ReadLine(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out field))
            {
                return false;
            }

            return true;
        }

        public List<BorrowBook> FindBorrowBookWithBookAndReader(Func<BorrowBook, bool> predicate)
        {
            return unitOfWork.BorrowBookRepository.Find(predicate)
                .Join(unitOfWork.BookRepository.GetAll(), br => br.BookId, b => b.BookId,
                (br, b) => new { br, b })
                .Join(unitOfWork.ReaderRepository.GetAll(), join => join.br.ReaderId, rd => rd.ReaderId,
                (join, rd) => new BorrowBook(join.br.Id, join.br.BorrowDate, join.br.ReturnDate, join.br.BookId, join.br.ReaderId, join.b, rd))
                .ToList();
        }

        public List<Review> FindReviewWithBookAndReader(Func<Review, bool> predicate)
        {
            return unitOfWork.ReviewRepository.Find(predicate)
                .Join(unitOfWork.BookRepository.GetAll(), r => r.BookId, b => b.BookId, 
                (r, b) => new { r, b})
                .Join(unitOfWork.ReaderRepository.GetAll(), join=> join.r.ReaderId, reader=> reader.ReaderId,
                (join, reader) => new Review(join.r.ReviewId, join.r.Title, join.r.Content, join.r.RankStar, join.r.BookId, join.r.ReaderId, join.b, reader))
                .ToList();
        }

        public void DisplayBooks()
        {
            Console.Clear();

            if (unitOfWork.BookRepository.Count == 0)
            {
                Console.WriteLine("No books available. Press any key to back to main menu");
                Console.ReadKey();
                return;
            }

            string option = "";
            int pageNum = 1;
            int maxNumPage = (int)Math.Ceiling((double)unitOfWork.BookRepository.Count / Constant.NUM_ROW_PAGE);

            do
            {
                try
                {
                    Console.Clear();

                    Console.WriteLine($"Page {pageNum}/{maxNumPage}");
                    unitOfWork.BookRepository.Display(Helper.GetPage<Book>(pageNum, unitOfWork.BookRepository.GetAll()));
                    ViewBookMenu();
                    
                    if (!InputAndValidateStringField("Enter your option: ", out option))
                    {
                        throw new Exception("Invalid input");
                    }

                    switch (option.ToUpper())
                    {
                        case "N":
                            if (pageNum == maxNumPage)
                            {
                                throw new Exception("Reach final page");
                            }
                            pageNum++;
                            continue;
                        case "P":
                            if (pageNum == 1)
                            {
                                throw new Exception("Reach first page");
                            }
                            pageNum--;
                            continue;
                        case "B":
                            continue;
                        default:
                            break;
                    }

                    int bookId = 0;

                    if (!int.TryParse(option, out bookId))
                    {
                        throw new Exception("Invalid input");
                    }
                    Book selectedBook = unitOfWork.BookRepository.GetValue(bookId);

                    if (selectedBook == null)
                    {
                        throw new Exception($"Cannot find any book with ID = {bookId}");
                    }
                    ViewDetailBook(selectedBook);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (string.IsNullOrEmpty(option) || !option.ToUpper().Equals("B"));

            void ViewDetailBook(Book selectedBook)
            {
                Console.Clear();

                int option = 0;

                do
                {
                    try
                    {
                        Console.Clear();
                        unitOfWork.BookRepository.Display(new List<Book> { selectedBook });
                        DetailBookMenu();

                        if (!InputAndValidateIntField("Enter your option: ", out option))
                        {
                            throw new Exception("Invalid input");
                        }

                        if (option > 6 || option < 1)
                        {
                            throw new Exception("Invalid input");
                        }

                        switch (option)
                        {
                            case 1:
                                EditBookInfo(selectedBook);
                                break;
                            case 2:
                                bool isDeleted = DeleteBook(selectedBook);

                                if (isDeleted)
                                {
                                    option = 6;
                                }
                                break;
                            case 3:
                                AddBookReview(selectedBook);
                                break;
                            case 4:
                                DisplayBookReviews(selectedBook);
                                break;
                            case 5:
                                BorrowBooks(selectedBook);
                                break;
                            case 6:
                                continue;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                    }
                } while (option != 6);

                void EditBookInfo(Book selectedBook)
                {
                    bool isValid = true;
                    string title = "";
                    string author = "";
                    string publicationYear = "";
                    string genre = "";
                    int quantity = 0;

                    do
                    {
                        try
                        {
                            Console.Clear();

                            unitOfWork.BookRepository.Display(new List<Book> { selectedBook });
                            isValid = true;

                            if (!(isValid = InputAndValidateStringField("Enter book title: ", out title)))
                            {
                                throw new Exception("Invalid input");
                            }

                            if (!(isValid = InputAndValidateStringField("Enter book author: ", out author)))
                            {
                                throw new Exception("Invalid input");
                            }

                            if (!(InputAndValidateStringField("Enter book publication year: ", out publicationYear)))
                            {
                                throw new Exception("Invalid input");
                            };

                            if (!(isValid = InputAndValidateStringField("Enter book genre: ", out genre)))
                            {
                                throw new Exception("Invalid input");
                            }

                            if (!(isValid=InputAndValidateIntField("Enter book quantity: ", out quantity)))
                            {
                                throw new Exception("Invalid input");
                            }

                            selectedBook.Title = title;
                            selectedBook.Author = author;
                            selectedBook.PublicationYear = publicationYear;
                            selectedBook.Genre = genre;
                            selectedBook.Quantity = quantity;
                            unitOfWork.BookRepository.Update(selectedBook);

                            Console.WriteLine("Update successfully. Press any key to continue");
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                    } while (!isValid);
                }

                bool DeleteBook(Book deletedBook)
                {
                    bool isValid = true;
                    string deleted;

                    do
                    {
                        try
                        {
                            Console.Clear();

                            unitOfWork.BookRepository.Display(new List<Book> { selectedBook });
                            isValid = true;

                            if (!(isValid=InputAndValidateStringField("Are you sure want to delete this book? (y/n): ", out deleted)))
                            {
                                throw new Exception("Invalid input");
                            }

                            if (deleted.ToLower() != "y" && deleted.ToLower() != "n")
                            {
                                throw new Exception("Invalid input");
                            }

                            switch (deleted)
                            {
                                case "y":
                                    List<BorrowBook> borrowBooks = unitOfWork.BorrowBookRepository.Find(x => x.BookId == selectedBook.BookId && x.ReturnDate > DateTime.Now);
                                    if (borrowBooks.Count > 0)
                                    {
                                        Console.WriteLine("Cannot delete this book at this time");
                                        Console.ReadKey();
                                        return false;
                                    }
                                    List<Review> reviews = unitOfWork.ReviewRepository.Find(x => x.BookId == deletedBook.BookId);

                                    unitOfWork.ReviewRepository.RemoveRange(reviews);
                                    borrowBooks = unitOfWork.BorrowBookRepository.Find(x => x.BookId == deletedBook.BookId);
                                    unitOfWork.BorrowBookRepository.RemoveRange(borrowBooks);
                                    unitOfWork.BookRepository.Remove(selectedBook);

                                    Console.WriteLine("Deleted successfully. Press any key to continue");
                                    Console.ReadKey();
                                    break;
                                case "n":
                                    return false;
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                    } while (!isValid);

                    return true;
                }

                void AddBookReview(Book reviewedBoook)
                {
                    Console.Clear();

                    bool isValid = true;
                    string title = "";
                    string content = "";
                    int rankStar = 0;

                    do
                    {
                        try
                        {
                            isValid = true;

                            if (!(isValid=InputAndValidateStringField("Enter review title: ", out title)))
                            {
                                throw new Exception("Invalid input");
                            }

                            if (!(isValid = InputAndValidateStringField("Enter review content: ", out content)))
                            {
                                throw new Exception("Invalid input");
                            }

                            if (!(isValid = InputAndValidateIntField("Enter review rank (0 - 5): ", out rankStar)))
                            {
                                throw new Exception("Invalid input");
                            }

                            if (rankStar > 5 || rankStar < 0)
                            {
                                isValid = false;
                                throw new Exception("Invalid input");
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                    } while (!isValid);

                    int readerId;

                    do
                    {
                        try
                        {
                            isValid = true;

                            unitOfWork.ReaderRepository.DisplayAll();

                            if (!(isValid=InputAndValidateIntField("Enter reader ID: ", out readerId)))
                            {
                                throw new Exception("Invalid input");
                            }
                            Reader reader = unitOfWork.ReaderRepository.GetValue(readerId);

                            if (reader == null)
                            {
                                isValid = false;
                                throw new Exception($"Cannot find any reader with ID = {readerId}");
                            }

                            int id = unitOfWork.ReviewRepository.GetLastId();
                            Review review = new Review(++id, title, content, rankStar, selectedBook.BookId, reader.ReaderId);
                            unitOfWork.ReviewRepository.Add(review);

                            Console.WriteLine("Add successfully. Press any key to continue");
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                    } while (!isValid);
                }

                void DisplayBookReviews(Book selectedBook)
                {
                    List<Review> reviews = FindReviewWithBookAndReader(x=>x.ReviewId == selectedBook.BookId);

                    if(reviews.Count == 0)
                    {
                        Console.WriteLine("No reviews found. Press any key to continue");
                        Console.ReadKey();

                        return;
                    }
                    unitOfWork.ReviewRepository.Display(reviews);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }

                void BorrowBooks(Book selectedBook)
                {
                    Console.Clear();

                    unitOfWork.BookRepository.Display(new List<Book> { selectedBook });
                    bool isValid = true;
                    DateTime returnDate = new DateTime();

                    if(selectedBook.Quantity == 0)
                    {
                        Console.WriteLine("This book is out of stock");
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();

                        return;
                    }

                    do
                    {
                        try
                        {
                            isValid = true;

                            if (!(isValid=InputAndValidateDateTimeField("Enter return date (Follow format dd/MM/yyy): ", out returnDate)))
                            {
                                throw new Exception("Invalid input");
                            }

                            if(returnDate <= DateTime.Now)
                            {
                                isValid = false;
                                throw new Exception("Invalid input");
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                    } while (!isValid);

                    int readerId = 0;
                    bool isNewReader = false;
                    Reader reader;
                    BorrowBook borrowBook;

                    do
                    {
                        try
                        {
                            isValid = true;

                            if (!(InputAndValidateIntField("Enter reader ID (Enter \"0\" to create new reader): ", out readerId)))
                            {
                                throw new Exception("Invalid input");
                            }

                            if (readerId == 0)
                            {
                                reader = HandleCeateReader();
                                isNewReader = true;
                            }
                            else
                            {
                                reader = unitOfWork.ReaderRepository.GetValue(readerId);

                                if(reader == null)
                                {
                                    throw new Exception($"Cannot find any reader with ID = {readerId}");
                                }                                
                            }

                            int borrowBookId = unitOfWork.BorrowBookRepository.GetLastId();
                            borrowBook = new BorrowBook(++borrowBookId, DateTime.Now, returnDate, selectedBook.BookId, reader.ReaderId);
                            selectedBook.Quantity--;

                            unitOfWork.BorrowBookRepository.Add(borrowBook);
                            unitOfWork.BookRepository.Update(selectedBook);

                            if (isNewReader)
                            {
                                unitOfWork.ReaderRepository.Add(reader);
                            }

                            Console.WriteLine("Borrow successfully. Press any key to continue");
                            Console.ReadKey();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                        }
                    } while (!isValid);
                }
            }
        }

        public void AddBook()
        {
            Console.Clear();

            bool isValid = true;
            string title;
            string author;
            string publicationYear;
            string genre;
            int quantity;

            do
            {
                try
                {
                    isValid = true;

                    if (!(isValid = InputAndValidateStringField("Enter book title: ", out title)))
                    {
                        throw new Exception("Invalid input");
                    }

                    if(!(isValid=InputAndValidateStringField("Enter book author: ", out author)))
                    {
                        throw new Exception("Invalid input");
                    }

                    if(!(isValid=InputAndValidateStringField("Enter book publication year: ", out publicationYear)))
                    {
                        throw new Exception("Invalid input");
                    }

                    if (!(isValid = InputAndValidateStringField("Enter book genre: ", out genre)))
                    {
                        throw new Exception("Invalid input");
                    }

                    if (!(isValid= InputAndValidateUnsignedIntField("Enter book quantity: ", out quantity)))
                    {
                        throw new Exception("Invalid input");
                    }

                    int id = unitOfWork.BookRepository.GetLastId();
                    Book newBook = new Book(++id, title, author, publicationYear, quantity, genre);
                    unitOfWork.BookRepository.Add(newBook);

                    Console.WriteLine("Add successfully. Press any key to continue");
                    Console.ReadKey();
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (!isValid);
        }

        public void FilterBooks()
        {
            bool isValid = true;
            int option = 0;
            IEnumerable<IGrouping<string, Book>> filterList;

            do
            {
                try
                {
                    Console.Clear();

                    isValid = true;

                    FilterBooksMenu();

                    if(!(isValid=InputAndValidateUnsignedIntField("Enter your option: ", out option))){
                        throw new Exception("Invalid input");
                    }

                    if(option > 6)
                    {
                        throw new Exception("Invalid input");
                    }

                    switch (option)
                    {
                        case 1:
                            filterList = unitOfWork.BookRepository.Filter(x => x.Title).OrderBy(x => x.Key);

                            foreach(var group in filterList)
                            {
                                Console.WriteLine($"Title: {group.Key}");
                                unitOfWork.BookRepository.Display(group.ToList<Book>());
                            }

                            Console.WriteLine();
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                            break;
                        case 2:
                            filterList = unitOfWork.BookRepository.Filter(x => x.Author).OrderBy(x => x.Key);

                            foreach (var group in filterList)
                            {
                                Console.WriteLine($"Author: {group.Key}");
                                unitOfWork.BookRepository.Display(group.ToList<Book>());
                            }

                            Console.WriteLine();
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                            break;
                        case 3:
                            filterList = unitOfWork.BookRepository.Filter(x => x.PublicationYear).OrderBy(x => x.Key);

                            foreach (var group in filterList)
                            {
                                Console.WriteLine($"Publication year: {group.Key}");
                                unitOfWork.BookRepository.Display(group.ToList<Book>());
                            }

                            Console.WriteLine();
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                            break;
                        case 4:
                            filterList = unitOfWork.BookRepository.Filter(x => x.Genre).OrderBy(x => x.Key);

                            foreach (var group in filterList)
                            {
                                Console.WriteLine($"Genre: {group.Key}");
                                unitOfWork.BookRepository.Display(group.ToList<Book>());
                            }

                            Console.WriteLine();
                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (!isValid || option != 5);
        }

        public void DisplayBorrowBook()
        {
            Console.Clear();

            if (unitOfWork.BorrowBookRepository.Count == 0)
            {
                Console.WriteLine("No books have been borrowed. Press any key to back to main menu");
                Console.ReadKey();
                return;
            }

            string option = "";
            int numPage = 1;
            int maxNumPage = (int)Math.Ceiling((double)unitOfWork.BorrowBookRepository.Count / Constant.NUM_ROW_PAGE);

            do
            {
                try
                {
                    Console.Clear();

                    Console.WriteLine($"Page {numPage}/{maxNumPage}");
                    unitOfWork.BorrowBookRepository.Display(Helper.GetPage(numPage, FindBorrowBookWithBookAndReader(x=>true)));
                    BorrowBookMenu();

                    if(!InputAndValidateStringField("Enter your option: ", out option))
                    {
                        throw new Exception("Invalid input");
                    }

                    switch (option.ToUpper())
                    {
                        case "N":
                            if (numPage == maxNumPage)
                            {
                                throw new Exception("Reach final page");
                            }
                            numPage++;
                            continue;
                        case "P":
                            if (numPage == 1)
                            {
                                throw new Exception("Reach first page");
                            }
                            numPage--;
                            continue;
                        case "B":
                            continue;
                        default:
                            break;
                    }

                    List<BorrowBook> filterBorrowBooks = FindBorrowBookWithBookAndReader(x => true).Where(x => x.Book.Title.Contains(option)).ToList();

                    if (filterBorrowBooks.Count == 0){
                        filterBorrowBooks = FindBorrowBookWithBookAndReader(x => true).Where(x => x.Reader.FullName.Contains(option)).ToList();

                        if(filterBorrowBooks.Count == 0)
                        {
                            int borrowBookId;

                            if(!int.TryParse(option, out borrowBookId))
                            {
                                throw new Exception($"Cannot find any borrow book with Book title/Reader fullname = {option}");
                            }
                            var returnBorrowBook = unitOfWork.BorrowBookRepository.GetValue(borrowBookId);

                            if(returnBorrowBook == null)
                            {
                                throw new Exception($"Cannot find any borrow book with ID = {option}");
                            }

                            unitOfWork.BorrowBookRepository.Remove(returnBorrowBook);

                            Console.WriteLine("Return book successfully. Press any key to continue");
                            Console.ReadKey();
                            continue;
                        }
                    }

                    Console.WriteLine("Filter borrow books:");
                    unitOfWork.BorrowBookRepository.Display(filterBorrowBooks);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (string.IsNullOrEmpty(option) || option.ToUpper() != "B");
        }

        public void ManageReaders()
        {
            bool isValid = true;
            int option = 0;

            do
            {
                try
                {
                    Console.Clear();

                    ManageReadersMenu();
                    isValid = true;

                    if(!(isValid=InputAndValidateIntField("Choose your option: ", out option)))
                    {
                        throw new Exception("Invalid input");
                    }

                    if(option < 1 || option > 5)
                    {
                        throw new Exception("Invalid input");
                    }

                    switch (option)
                    {
                        case 1:
                            Reader newReader = HandleCeateReader();
                            unitOfWork.ReaderRepository.Add(newReader);
                            Console.WriteLine("Add successfully. Press any key to continue");
                            Console.ReadKey();
                            break;
                        case 2:
                            HandleViewReader();
                            break;
                        case 3:
                            Reader updatedReader = HandleUpdateReader();
                            unitOfWork.ReaderRepository.Update(updatedReader);

                            Console.WriteLine("Update successfully. Press any key to continue");
                            Console.ReadKey();
                            break;
                        case 4:
                            HandleDeleteReader();
                            break;
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (!isValid || option != 5);
        }

        Reader HandleCeateReader()
        {
            bool isValid = true;
            string fullName;
            int age;
            DateTime birthDate;

            do
            {
                try
                {
                    isValid = true;

                    if(!(isValid=InputAndValidateStringField("Enter reader full name: ", out fullName)))
                    {
                        throw new Exception("Invalid input");
                    }

                    if (!(isValid = InputAndValidateIntField("Enter reader age: ", out age)))
                    {
                        throw new Exception("Invalid input");
                    }

                    if (!(isValid = InputAndValidateDateTimeField("Enter reader birth date (Follow format dd/MM/yyy): ", out birthDate)))
                    {
                        throw new Exception("Invalid input");
                    }

                    int id = unitOfWork.ReaderRepository.GetLastId();
                    return new Reader(++id, fullName, age, birthDate);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (!isValid);

            return null;
        }

        void HandleViewReader()
        {
            Console.Clear();

            if (unitOfWork.ReaderRepository.Count == 0)
            {
                Console.WriteLine("No reader available. Press any key to back to main menu");
                Console.ReadKey();
                return;
            }

            string option = "";
            int pageNum = 1;
            int maxNumPage = (int)Math.Ceiling((double)unitOfWork.ReaderRepository.Count / Constant.NUM_ROW_PAGE);

            do
            {
                try
                {
                    Console.Clear();

                    Console.WriteLine($"Page {pageNum}/{maxNumPage}");
                    unitOfWork.ReaderRepository.Display(Helper.GetPage<Reader>(pageNum, unitOfWork.ReaderRepository.GetAll()));
                    ViewReaderMenu();

                    if (!InputAndValidateStringField("Enter your option: ", out option))
                    {
                        throw new Exception("Invalid input");
                    }

                    switch (option.ToUpper())
                    {
                        case "N":
                            if (pageNum == maxNumPage)
                            {
                                throw new Exception("Reach final page");
                            }
                            pageNum++;
                            break;
                        case "P":
                            if (pageNum == 1)
                            {
                                throw new Exception("Reach first page");
                            }
                            pageNum--;
                            break;
                        case "B":
                            continue;
                        default:
                            break;
                    }

                    if(option.ToUpper() != "N" && option.ToUpper() != "P" && option.ToUpper() != "B")
                    {
                        throw new Exception("Invalid input");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (string.IsNullOrEmpty(option) || !option.ToUpper().Equals("B"));
        }

        Reader HandleUpdateReader()
        {
            Console.Clear();

            bool isValid = true;
            string fullName = "";
            int age = 0;
            DateTime birthDate = new DateTime();
            int readerId = 0;
            Reader updatedReader;

            do
            {
                try
                {
                    Console.Clear();

                    isValid = true;

                    unitOfWork.ReaderRepository.DisplayAll();
                    Console.Write("Enter reader ID: ");

                    if(!(isValid=InputAndValidateUnsignedIntField("Enter reader ID: ", out readerId)))
                    {
                        throw new Exception("Invalid input");
                    }
                    updatedReader = unitOfWork.ReaderRepository.GetValue(readerId);

                    if(updatedReader == null)
                    {
                        isValid = false;
                        throw new Exception("Invalid input");
                    }

                    if(!(isValid=InputAndValidateStringField("Enter reader full name: ", out fullName)))
                    {
                        throw new Exception("Invalid input");
                    }

                    if(!(isValid=InputAndValidateUnsignedIntField("Enter reader age: ", out age)))
                    {
                        throw new Exception("Invalid input");
                    }

                    if(!(isValid=InputAndValidateDateTimeField("Enter reader birth date: ", out birthDate)))
                    {
                        throw new Exception("Invalid input");
                    }

                    updatedReader.FullName = fullName;
                    updatedReader.Age = age;
                    updatedReader.BirthDate = birthDate;

                    return updatedReader;
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (!isValid);

            return null;
        }

        void HandleDeleteReader()
        {
            bool isValid = true;
            string option = "";
            int readerId = 0;
            Reader deletedReader;

            do
            {
                try
                {
                    Console.Clear();

                    isValid = true;

                    unitOfWork.ReaderRepository.DisplayAll();

                    if(!(isValid=InputAndValidateUnsignedIntField("Enter reader ID: ", out readerId)))
                    {
                        throw new Exception("Invalid input");
                    }
                    deletedReader = unitOfWork.ReaderRepository.GetValue(readerId);

                    if(deletedReader == null)
                    {
                        isValid = false;
                        throw new Exception("Invalid input");
                    }

                    if(!(isValid=InputAndValidateStringField("Are your sure want to delete this Reader? (y/n): ", out option)))
                    {
                        throw new Exception("Invalid input");
                    }

                    if(option.ToLower() != "y" && option.ToLower() != "n")
                    {
                        isValid = false;
                        throw new Exception("Invalid input");
                    }

                    switch (option.ToLower())
                    {
                        case "y":
                            unitOfWork.ReaderRepository.Remove(deletedReader);
                            Console.WriteLine("Deleted successfully. Press any key to continue");
                            Console.ReadKey();
                            break;
                        case "n":
                            break;
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (!isValid);
        }
    }
}
