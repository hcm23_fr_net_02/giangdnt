﻿using System.Text.Json;

namespace GiangDNT_MiniProject_LibraryManagementSystem
{
    public class JsonHandler<T>
    {
        public static List<T> ReadArrayJson(string path)
        {

            string content = File.ReadAllText(path);

            using FileStream openStream = File.OpenRead(path);

            if(openStream.Length == 0)
            {
                return new List<T>();
            }

            List<T> array = JsonSerializer.Deserialize<List<T>>(openStream);

            return array;
        }

        public static void WriteArrayJson(string path, List<T> array)
        {
            using FileStream createStream = File.Create(path);
            JsonSerializer.Serialize<List<T>>(createStream, array);
            createStream.Dispose();
        }
    }
}
