﻿using GiangDNT_MiniProject_LibraryManagementSystem.Repositories;

namespace GiangDNT_MiniProject_LibraryManagementSystem
{
    public interface IUnitOfWork
    {
        public IBookRepository BookRepository { get; }
        public IReaderRepository ReaderRepository { get; }
        public IReviewRepository ReviewRepository { get; }
        public IBorrowBookRepository BorrowBookRepository { get; }
    }

    public class UnitOfWork : IUnitOfWork
    {
        private readonly IBookRepository _bookRepository;
        private readonly IReaderRepository _readerRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly IBorrowBookRepository _borrowBookRepository;

        public UnitOfWork()
        {
            _bookRepository = new BookRepository();
            _readerRepository = new ReaderRepository();
            _reviewRepository = new ReviewRepository();
            _borrowBookRepository = new BorrowBookRepository();
        }

        public IBookRepository BookRepository { get => _bookRepository; }
        public IReaderRepository ReaderRepository { get => _readerRepository; }
        public IReviewRepository ReviewRepository { get => _reviewRepository; }
        public IBorrowBookRepository BorrowBookRepository { get => _borrowBookRepository; }
    }
}
