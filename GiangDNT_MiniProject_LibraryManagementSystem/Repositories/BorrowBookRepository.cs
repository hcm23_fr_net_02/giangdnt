﻿using GiangDNT_MiniProject_LibraryManagementSystem.Models;

namespace GiangDNT_MiniProject_LibraryManagementSystem.Repositories
{
    public interface IBorrowBookRepository : IGenericRepository<BorrowBook>
    {
        
    }

    public class BorrowBookRepository : GenericRepository<BorrowBook>, IBorrowBookRepository
    {
        public BorrowBookRepository()
        {
            _dbSet = JsonHandler<BorrowBook>.ReadArrayJson(Constant.BORROWBOOK_PATH);
        }

        public override BorrowBook GetValue(int id)
        {
            return _dbSet.SingleOrDefault(x => x.Id == id);
        }

        public override void Update(BorrowBook entity)
        {
            int index = _dbSet.FindIndex(0, x => x.Id == entity.Id);

            if (index == -1)
            {
                return;
            }

            _dbSet[index] = entity;
        }

        public override void UpdateRange(List<BorrowBook> tList)
        {
            foreach (var borrowBook in tList)
            {
                int index = _dbSet.FindIndex(0, x => x.Id == borrowBook.Id);
                _dbSet[index] = borrowBook;
            }
        }

        public override void Display(List<BorrowBook> tList)
        {
            foreach (BorrowBook borrowBook in tList)
            {
                Console.WriteLine($"ID: {borrowBook.Id} - Borrowed date: {borrowBook.BorrowDate.ToString("dd/MM/yyy")} - Returned date: {borrowBook.ReturnDate.ToString("dd/MM/yyy")} - Book: {borrowBook.Book.Title} - Reader: {borrowBook.Reader.FullName}");
            }
            Console.WriteLine();
        }

        public override void DisplayAll()
        {
            foreach (BorrowBook borrowBook in _dbSet)
            {
                Console.WriteLine($"ID: {borrowBook.Id} - Borrowed date: {borrowBook.BorrowDate.ToString("dd/MM/yyy")} - Returned date: {borrowBook.ReturnDate.ToString("dd/MM/yyy")} - Book: {borrowBook.Book.Title} - Reader: {borrowBook.Reader.FullName}");
            }
            Console.WriteLine();
        }

        public override int GetLastId()
        {
            var item = _dbSet.LastOrDefault();

            if (item == null)
            {
                return 0;
            }

            return item.Id;
        }
    }
}
