﻿namespace GiangDNT_MiniProject_LibraryManagementSystem.Repositories
{
    public interface IGenericRepository<T> where T : class
    {
        int Count { get; }

        T GetValue(int id);
        List<T> GetAll();
        List<T> Find(Func<T, bool> predicate);

        void Add(T entity);
        void AddRange(List<T> tList);

        void Update(T entity);  
        void UpdateRange(List<T> tList);

        void Remove(T entity);
        void RemoveRange(List<T> tList);

        void Display(List<T> tList);
        void DisplayAll();

        int GetLastId();

        IEnumerable<IGrouping<TKey, T>> Filter<TKey>(Func<T, TKey> keySelector);
    }

    public abstract class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected List<T> _dbSet;
        public int Count { get => _dbSet.Count; }

        public GenericRepository()
        {
        }

        public abstract T GetValue(int id);

        public List<T> GetAll()
        {
            return _dbSet;
        }

        public List<T> Find(Func<T, bool> predicate)
        {
            if(predicate == null)
            {
                return _dbSet;
            }

            return _dbSet.Where(predicate).ToList();
        }

        public void Add(T entity)
        {
            if(entity != null)
            {
                _dbSet.Add(entity);
            }
        }

        public void AddRange(List<T> tList)
        {
            _dbSet.AddRange(tList);
        }

        public abstract void Update(T entity);

        public abstract void UpdateRange(List<T> tList);

        public void Remove(T entity)
        {
            _dbSet.Remove(entity);
        }

        public void RemoveRange(List<T> tList)
        {
            foreach(var item in tList)
            {
                _dbSet.Remove(item);
            }
        }

        public abstract void Display(List<T> tList);

        public abstract void DisplayAll();

        public abstract int GetLastId();

        public IEnumerable<IGrouping<TKey, T>> Filter<TKey>(Func<T, TKey> keySelector)
        {
            return _dbSet.GroupBy(keySelector);
        }
    }
}
