﻿using GiangDNT_MiniProject_LibraryManagementSystem.Models;

namespace GiangDNT_MiniProject_LibraryManagementSystem.Repositories
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        
    }

    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        public BookRepository()
        {
            _dbSet = JsonHandler<Book>.ReadArrayJson(Constant.BOOK_PATH);
        }

        public override Book GetValue(int id)
        {
            return _dbSet.SingleOrDefault(x => x.BookId == id);
        }

        public override void Update(Book entity)
        {
            int index = _dbSet.FindIndex(0, x => x.BookId == entity.BookId);
            
            if(index == -1)
            {
                return;
            }

            _dbSet[index] = entity;
        }

        public override void UpdateRange(List<Book> tList)
        {
            foreach(var book in tList)
            {
                int index = _dbSet.FindIndex(0, x => x.BookId == book.BookId);
                _dbSet[index] = book;
            }
        }

        public override void Display(List<Book> tList)
        {
            foreach (Book book in tList)
            {
                Console.WriteLine($"ID: {book.BookId} - Title: {book.Title} - Author: {book.Author} - Genre: {book.Genre} - Publication: {book.PublicationYear} - Quantity: {book.Quantity}");
            }
            Console.WriteLine();
        }

        public override void DisplayAll()
        {
            foreach (Book book in _dbSet)
            {
                Console.WriteLine($"ID: {book.BookId} - Title: {book.Title} - Author: {book.Author} - Genre: {book.Genre} - Publication: {book.PublicationYear} - Quantity: {book.Quantity}");
            }
            Console.WriteLine();
        }

        public override int GetLastId()
        {
            var item = _dbSet.LastOrDefault();

            if(item == null)
            {
                return 0;
            }

            return item.BookId;
        }
    }
}
