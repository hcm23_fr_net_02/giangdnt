﻿using GiangDNT_MiniProject_LibraryManagementSystem.Models;

namespace GiangDNT_MiniProject_LibraryManagementSystem.Repositories
{
    public interface IReaderRepository : IGenericRepository<Reader>
    {
        
    }

    public class ReaderRepository : GenericRepository<Reader>, IReaderRepository
    {
        public ReaderRepository() : base()
        {
            _dbSet = JsonHandler<Reader>.ReadArrayJson(Constant.READER_PATH);
        }

        public override Reader GetValue(int id)
        {
            return _dbSet.SingleOrDefault(x => x.ReaderId == id);
        }

        public override void Update(Reader entity)
        {
            int index = _dbSet.FindIndex(0, x => x.ReaderId == entity.ReaderId);

            if (index == -1)
            {
                return;
            }

            _dbSet[index] = entity;
        }

        public override void UpdateRange(List<Reader> tList)
        {
            foreach (var reader in tList)
            {
                int index = _dbSet.FindIndex(0, x => x.ReaderId == reader.ReaderId);
                _dbSet[index] = reader;
            }
        }

        public override void Display(List<Reader> tList)
        {
            foreach(Reader reader in tList)
            {
                Console.WriteLine($"ID: {reader.ReaderId} - Full name: {reader.FullName} - Age: {reader.Age} - Birth date: {reader.BirthDate.ToString("dd/MM/yyy")}");
            }
            Console.WriteLine();
        }

        public override void DisplayAll()
        {
            foreach (Reader reader in _dbSet)
            {
                Console.WriteLine($"ID: {reader.ReaderId} - Full name: {reader.FullName} - Age: {reader.Age} - Birth date: {reader.BirthDate.ToString("dd/MM/yyy")}");
            }
            Console.WriteLine();
        }

        public override int GetLastId()
        {
            var item = _dbSet.LastOrDefault();

            if (item == null)
            {
                return 0;
            }

            return item.ReaderId;
        }
    }
}
