﻿using GiangDNT_MiniProject_LibraryManagementSystem.Models;

namespace GiangDNT_MiniProject_LibraryManagementSystem.Repositories
{
    public interface IReviewRepository : IGenericRepository<Review>
    {

    }

    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
        public ReviewRepository() : base()
        {
            _dbSet = JsonHandler<Review>.ReadArrayJson(Constant.REVIEW_PATH);
        }

        public override Review GetValue(int id)
        {
            return _dbSet.SingleOrDefault(x => x.ReviewId == id);
        }

        public override void Update(Review entity)
        {
            int index = _dbSet.FindIndex(0, x => x.ReviewId == entity.ReviewId);

            if (index == -1)
            {
                return;
            }

            _dbSet[index] = entity;
        }

        public override void UpdateRange(List<Review> tList)
        {
            foreach (var review in tList)
            {
                int index = _dbSet.FindIndex(0, x => x.ReviewId == review.ReviewId);
                _dbSet[index] = review;
            }
        }

        public override void Display(List<Review> tList)
        {
            foreach(Review review in tList)
            {
                Console.WriteLine($"ID: {review.ReviewId} - Title: {review.Title} - Content: {review.Content} - Rank: {review.RankStar} - Book: {review.Book.Title} - Reader: {review.Reader.FullName}");
            }
            Console.WriteLine();
        }

        public override void DisplayAll()
        {
            foreach (Review review in _dbSet)
            {
                Console.WriteLine($"ID: {review.ReviewId} - Title: {review.Title} - Content: {review.Content} - Rank: {review.RankStar} - Book: {review.Book.Title} - Reader: {review.Reader.FullName}");
            }
            Console.WriteLine();
        }

        public override int GetLastId()
        {
            var item = _dbSet.LastOrDefault();

            if (item == null)
            {
                return 0;
            }

            return item.ReviewId;
        }
    }
}
