﻿namespace GiangDNT_MiniProject_LibraryManagementSystem
{
    public class Helper
    {
        public static List<T> GetPage<T>(int pageNum, List<T> tList, int numRowOfPage = Constant.NUM_ROW_PAGE)
        {
            return tList.Skip((pageNum - 1) * numRowOfPage).Take(numRowOfPage).ToList();
        }
    }
}
