﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using GiangDNT_Stack;

namespace StackUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod_Stack_PushValueToStack()
        {
            //assert
            Stack s = new Stack();
            var param = 1;

            //act
            s.Push(param);

            //arrange
            Assert.IsTrue(s.Count == 1);
        }
        [TestMethod]
        public void TestMethod_Stack_PushManyValuesToStack()
        {
            //assert
            Stack s = new Stack();
            var param = 1;

            //act
            s.Push(param);
            s.Push(param);
            s.Push(param);

            //arrange
            Assert.IsTrue(s.Count == 3);
        }
        [TestMethod]
        public void TestMethod_Stack_PushValueToStack_WhenStackIsFull()
        {
            //assert
            Stack s = new Stack();
            var param = 1;

            //act
            for(int i = 0; i < 11; i++)
            {
                s.Push(param);
            }

            //arrange
            Assert.AreEqual(s.Count, 10);
        }
        [TestMethod]
        public void TestMethod_Stack_RemoveValueFromStack()
        {
            //assert
            Stack s = new Stack();
            var expected = 1;

            //act
            s.Push(1);
            var actual = s.Pop();

            //arrange
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethod_Stack_RemoveValueFromStack_WhenStackIsEmpty()
        {
            //assert
            Stack s = new Stack();
            var expected = 0;

            //act
            var actual = s.Pop();

            //arrange
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethod_Stack_GetValueFromPeakOfStack()
        {
            //assert
            Stack s = new Stack();
            var expected = 5;

            //act
            s.Push(5);
            var actual = s.Peak();

            //arrange
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethod_Stack_GetValueFromPeakOfStack_WhenStackIsEmpty()
        {
            //assert
            Stack s = new Stack();
            var expected = 0;

            //act
            var actual = s.Peak();

            //arrange
            Assert.AreEqual(expected, actual);
        }
    }
}
