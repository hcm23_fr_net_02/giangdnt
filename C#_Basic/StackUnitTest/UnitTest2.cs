﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using GiangDNT_Stack;

namespace QueueUnitTest
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestMethod_Queue_EnqueueValueToQueue()
        {
            //assert
            Queue q = new Queue();
            var param = 1;

            //act
            q.Enqueue(param);

            //arrange
            Assert.IsTrue(q.Count == 1);
        }
        [TestMethod]
        public void TestMethod_Queue_EnqueueManyValuesToQueue()
        {
            //assert
            Queue q = new Queue();
            var param = 1;

            //act
            q.Enqueue(param);
            q.Enqueue(param);
            q.Enqueue(param);

            //arrange
            Assert.IsTrue(q.Count == 3);
        }
        [TestMethod]
        public void TestMethod_Queue_EnqueueValueToQueue_WhenQueueIsFull()
        {
            //assert
            Queue q = new Queue();
            var param = 1;

            //act
            for (int i = 0; i < 11; i++)
            {
                q.Enqueue(param);
            }

            //arrange
            Assert.AreEqual(q.Count, 10);
        }
        [TestMethod]
        public void TestMethod_Queue_RemoveValueFromQueue()
        {
            //assert
            Queue q = new Queue();
            var expected = 1;

            //act
            q.Enqueue(1);
            var actual = q.Dequeue();

            //arrange
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethod_Queue_RemoveValueFromQueue_WhenQueueIsEmpty()
        {
            //assert
            Queue q = new Queue();
            var expected = 0;

            //act
            var actual = q.Dequeue();

            //arrange
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethod_Queue_GetValueFromPeakOfQueue()
        {
            //assert
            Queue q = new Queue();
            var expected = 5;

            //act
            q.Enqueue(5);
            var actual = q.Peak();

            //arrange
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethod_Queue_GetValueFromPeakOfQueue_WhenQueueIsEmpty()
        {
            //assert
            Queue q = new Queue();
            var expected = 0;

            //act
            var actual = q.Peak();

            //arrange
            Assert.AreEqual(expected, actual);
        }
    }
}
