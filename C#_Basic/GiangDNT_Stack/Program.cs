﻿using System;

namespace GiangDNT_Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue q = new Queue();
            q.Enqueue(5);
            q.Enqueue(12);
            q.Enqueue(3);
            q.Enqueue(35);
            q.Enqueue(55);
            Console.WriteLine(q.Peak());
            Console.WriteLine(q.Dequeue()); 
            Console.WriteLine(q.Dequeue());
            Console.WriteLine(q.Peak());
            Console.WriteLine(q.Dequeue());
            Console.WriteLine(q.Dequeue());
            Console.WriteLine(q.Dequeue());
            Console.WriteLine(q.Dequeue());
            Console.WriteLine(q.Peak());
            Console.ReadKey();
        }
    }

    public class Constants
    {
        public const int SIZE = 5;
    }

    public class Stack
    {
        private int[] stack;
        public int Count 
        { 
            get
            {
                return count;
            }
        }
        private int count;
        private int index;

        public Stack()
        {
            stack = new int[Constants.SIZE];
            count = 0;
            index = -1;
        }

        public void Push(int item)
        {
            if(count < Constants.SIZE)
            {
                count++;

                stack[++index] = item;
            }
        }

        public int Pop()
        {
            if(count > 0)
            {
                count--;

                return stack[index--];
            }

            return 0;
        }

        public int Peak()
        {
            if(count > 0)
                return stack[index];

            return 0;
        }
    }

    public class Queue
    {
        private int[] queue;
        private int front;
        private int back;
        private int count;
        public int Count
        {
            get
            {
                return count;
            }
        }

        public Queue()
        {
            queue = new int[Constants.SIZE];
            count = 0;
            front = -1;
            back = -1;
        }

        public void Enqueue(int item)
        {
            if(count < Constants.SIZE)
            {
                if (front == -1)
                    front++;
                queue[++back] = item;
                count++;
            }

        }

        public int Dequeue()
        {
            if(count > 0)
            {
                int deqe_val = queue[front];
                for(int i = front; i < back; i++)
                {
                    queue[i] = queue[i + 1];
                }

                count--;
                back--;
                if (count == 0)
                    front--;

                return deqe_val;
            }

            return 0;
        }

        public int Peak()
        {
            if(count > 0)
            {
                return queue[front];
            }

            return 0;
        }
    }
}
