﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace GiangDNT_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            var employees = ReadArrayJson("D:\\Self-learning Stuffs\\Fresher .NET\\Assignments\\GiangDNT_LINQ\\mock_data.json");

            //Lấy ra danh sách các FirstName của tất cả các nhân viên.
            var names = employees.Select(x => x.FirstName);
            foreach(var name in names)
            {
                Console.WriteLine(name);
            }
            Console.WriteLine("========================================================================");
            //Lấy ra danh sách các nhân viên có Salary lớn hơn 50000$.
            var salary_emps = employees.Where(x => x.Salary > 50000);
            foreach(var item in salary_emps)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("========================================================================");
            //Lấy ra danh sách các nhân viên có Gender là "Male" và sắp xếp tăng dần theo FirstName.
            var male_emps = employees.Where(x => x.Gender == "Male").OrderBy(x => x.FirstName);
            foreach (var item in male_emps)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("========================================================================");
            //Lấy ra danh sách các nhân viên có Country là "Indonesia" và JobTitle chứa "Manager".
            var indo_mana_emps = employees.Where(x => !string.IsNullOrEmpty(x.Country) && !string.IsNullOrEmpty(x.JobTitle)).Where(x => x.Country == "Indonesia" && x.JobTitle.Contains("Manager"));
            foreach (var item in indo_mana_emps)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("========================================================================");
            //Lấy ra danh sách các nhân viên có Email và sắp xếp giảm dần theo LastName.
            var email_emps = employees.Where(x => !string.IsNullOrEmpty(x.Email)).Where(x => x.Email.Length > 0);
            foreach(var item in email_emps)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("========================================================================");
            //Lấy ra danh sách các nhân viên có StartDate trước ngày "2022-01-01" và Salary lớn hơn 60000$.
            var startdate_before_emps = employees.Where(x => x.StartDate?.CompareTo(new DateTime(2022,1,1,0,0,0)) < 0 && x.Salary > 60000);
            foreach(var item in startdate_before_emps)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("========================================================================");
            //Lấy ra danh sách các nhân viên có PostalCode là null hoặc rỗng và sắp xếp tăng dần theo LastName.
            var nil_postalcode_emps = employees.Where(x => string.IsNullOrEmpty(x.PostalCode)).OrderBy(x => x.LastName);
            foreach(var item in nil_postalcode_emps)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("========================================================================");
            //Lấy ra danh sách các nhân viên có FirstName và LastName được viết hoa và sắp xếp giảm dần theo Id.
            var upper_name_emps = employees.Where(x => x.FirstName.Equals(x.FirstName.ToUpper()) && x.LastName.Equals(x.LastName.ToUpper())).OrderByDescending(x => x.Id);
            foreach(var item in upper_name_emps)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("========================================================================");
            //Lấy ra danh sách các nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ và sắp xếp tăng dần theo Salary.
            var sal_between_emps = employees.Where(x => x.Salary >= 50000 && x.Salary <= 70000).OrderBy(x => x.Salary);
            foreach(var item in sal_between_emps)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("========================================================================");
            //Lấy ra danh sách các nhân viên có FirstName chứa chữ "a" hoặc "A" và sắp xếp giảm dần theo LastName.
            var name_contain_emps = employees.Where(x => x.FirstName.Contains('a') || x.FirstName.Contains('A')).OrderByDescending(x => x.LastName);
            foreach(var item in name_contain_emps)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }

        public static List<Employee> ReadArrayJson(string path)
        {
            var content = File.ReadAllText(path);
            var options = new JsonSerializerOptions();
            options.Converters.Add(new MyDateTimeJsonConverter());

            List<Employee> employees = JsonSerializer.Deserialize<List<Employee>>(content, options);

            return employees;
        }
    }

    public class MyDateTimeJsonConverter : JsonConverter<DateTime>
    {
        public override DateTime Read(
            ref Utf8JsonReader reader,
            Type typeToConvert,
            JsonSerializerOptions options) =>
                DateTime.ParseExact(reader.GetString()!,
                    "MM/dd/yyyy", CultureInfo.InvariantCulture);

        public override void Write(
            Utf8JsonWriter writer,
            DateTime dateTimeValue,
            JsonSerializerOptions options) =>
                writer.WriteStringValue(dateTimeValue.ToString(
                    "MM/dd/yyyy", CultureInfo.InvariantCulture));
    }

    class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string JobTitle { get; set; }
        public DateTime? StartDate { get; set; }
        public double Salary { get; set; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
