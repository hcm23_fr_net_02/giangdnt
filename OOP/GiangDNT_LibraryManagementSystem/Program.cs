﻿using GiangDNT_LibraryManagementSystem.Models;
using System.Globalization;
using System.Reflection.Metadata;

namespace GiangDNT_LibraryManagementSystem
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Library.Init();
            int option = 0;

            do
            {
                try
                {
                    Console.Clear();

                    MainMenu();
                    Console.Write("Choose your option: ");

                    if (!int.TryParse(Console.ReadLine(), out option))
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        continue;
                    }

                    if (option < 1 || option > 7)
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        continue;
                    }

                    switch (option)
                    {
                        case 1:
                            AddBook();
                            break;
                        case 2:
                            DeleteBook();
                            break;
                        case 3:
                            DisplayBooks();
                            break;
                        case 4:
                            BorrowBooks();
                            break;
                        case 5:
                            DisplayLoans();
                            break;
                        case 6:
                            FindBook();
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.ReadKey();
                }
            } while (option != 7);
        }

        static void MainMenu()
        {
            Console.WriteLine("*********************LIBRARY MANAGEMENT SYSTEM*********************");
            Console.WriteLine("1. Add new book");
            Console.WriteLine("2. Delete book");
            Console.WriteLine("3. Display books");
            Console.WriteLine("4. Borrow books");
            Console.WriteLine("5. Display borrowed books");
            Console.WriteLine("6. Find book");
            Console.WriteLine("7. Exit");
            Console.WriteLine();
        }

        static void AddBook()
        {
            Console.Clear();

            bool isValid = true;
            string title;
            string author;
            int quantity;
            double price;

            try
            {
                do
                {
                    Console.Clear();

                    isValid = true;
                    Console.Write("Enter book title: ");
                    title = Console.ReadLine();

                    if (string.IsNullOrEmpty(title))
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        isValid = false;
                        continue;
                    }
                    
                    Console.Write("Enter book author: ");
                    author = Console.ReadLine();

                    if (string.IsNullOrEmpty(author))
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        isValid = false;
                        continue;
                    }

                    Console.Write("Enter book quantity: ");

                    if (!int.TryParse(Console.ReadLine(), out quantity))
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        isValid = false;
                        continue;
                    }

                    Console.Write("Enter book price: ");

                    if (!double.TryParse(Console.ReadLine(), out price))
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        isValid = false;
                        continue;
                    }

                    Library.AddBook(new Book(++Library.BooksIndex, title, author, quantity, price));
                    Console.WriteLine("Add book successfully. Press any key to back to main menu");
                    Console.ReadKey();
                } while (!isValid);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }

        static void DeleteBook()
        {
            Console.Clear();

            bool isValid = true;
            int deletedBookId = 0;
            var deletedBook = new Book();
            Library.DisplayBooksAll();

            try
            {
                do
                {
                    isValid = true;
                    Console.Write("Enter book ID for deleting: ");
                    if (!int.TryParse(Console.ReadLine(), out deletedBookId))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }

                    deletedBook = Library.GetBookById(deletedBookId);

                    if (deletedBook == null)
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                } while (!isValid);

                Library.DeleteBook(deletedBook);
                Console.WriteLine("Deleted book successfully. Press any key to back to main menu");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }

        static void DisplayBooks()
        {
            Console.Clear();

            Library.DisplayBooksAll();
            Console.WriteLine("Press any key to back to main menu");
            Console.ReadKey();
        }

        static void BorrowBooks()
        {
            Console.Clear();

            bool isValid = true;
            int customerId;
            Customer customer = new Customer();
            int bookId;
            List<Book> loanBooks = new List<Book>();
            string strExpiredDate;
            DateTime expiredDate = new DateTime();

            Library.DisplayCustomers();

            try
            {
                do
                {
                    isValid = true;
                    Console.Write("Enter customer ID: ");

                    if (!int.TryParse(Console.ReadLine(), out customerId))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                    customer = Library.GetCustomerById(customerId);
                    if (customer == null)
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                } while (!isValid);

                Console.WriteLine();
                Library.DisplayBooksAll();

                do
                {
                    Console.Write("Enter book ID (Enter \"0\" to stop choosing): ");

                    if (!int.TryParse(Console.ReadLine(), out bookId))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }

                    if (bookId == 0)
                    {
                        isValid = true;
                        continue;
                    }
                    var loanBook = Library.GetBookById(bookId);

                    if (loanBook == null)
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }

                    if (loanBook.Quantity <= 0)
                    {
                        Console.WriteLine("This book is out of stock");
                        isValid = false;
                        continue;
                    }
                    
                    isValid = false;
                    loanBook.Quantity--;
                    Library.UpdateBook(bookId, loanBook);
                    loanBooks.Add(loanBook);
                } while (!isValid);

                if (loanBooks.Count == 0)
                {
                    Console.WriteLine("You didn't choose any book yet");
                    Console.WriteLine("Press any key to back to main menu");
                    Console.ReadKey();
                    return;
                }

                do
                {
                    isValid = true;
                    Console.Write("Enter expired date (Follow this format: yyyy-mm-dd): ");

                    if (string.IsNullOrEmpty(strExpiredDate = Console.ReadLine()))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                    if(!DateTime.TryParseExact(strExpiredDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out expiredDate))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                } while (!isValid);

                var loan = new Loan(++Library.LoansIndex, DateTime.Now, expiredDate, customer, loanBooks);
                Library.AddLoan(loan);
                Console.WriteLine("Borrowed books successfully. Press any key to back to main menu");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }

        static void DisplayLoans()
        {
            Console.Clear();

            Library.DisplayLoans();
            Console.WriteLine("Press any key to back to main menu");
            Console.ReadKey();
        }

        static void FindBook()
        {
            Console.Clear();

            bool isValid = true;
            int bookId;
            var book = new Book();

            try
            {
                do
                {
                    isValid = true; 

                    Console.Write("Enter book ID: ");

                    if (!int.TryParse(Console.ReadLine(), out bookId))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }

                    book = Library.GetBookById(bookId);

                    if (book == null)
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }

                    Console.WriteLine($"ID: {book.Id} - Title: {book.Title} - Author: {book.Author} - Quantity: {book.Quantity} - Price: {book.Price}$");
                } while (!isValid);                
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }

            Console.WriteLine("Press any key to back to main menu");
            Console.ReadKey();
        }
    }
}