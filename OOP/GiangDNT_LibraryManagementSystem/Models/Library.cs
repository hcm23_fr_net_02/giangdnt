﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_LibraryManagementSystem.Models
{
    internal class Library
    {
        private static List<Book> Books = new List<Book>();
        private static List<Loan> Loans = new List<Loan>();
        private static List<Customer> Customers = new List<Customer>();

        public static int BooksIndex = 0;
        public static int LoansIndex = 0;
        public static int Count { get=>Books.Count; }

        public static void Init()
        {
            Customers.Add(new Customer(1, "Billy Juliver", "New York, United State"));
            Customers.Add(new Customer(2, "Emma Watson", "New York, United State"));
            Customers.Add(new Customer(3, "Peter Walter", "New York, United State"));
            Customers.Add(new Customer(4, "Nick Heimer", "New York, United State"));
        }

        public static void AddBook(Book book)
        {
            Books.Add(book);
        }

        public static Book GetBookById(int id)
        {
            foreach(var book in Books) 
            { 
                if(book.Id == id) return book;
            }

            return null;
        }

        public static void UpdateBook(int id, Book updatedBook)
        {
            for (int i = 0; i < Count; i++)
            {
                if (Books[i].Id == id)
                {
                    Books[i] = updatedBook;
                    return;
                }
            }
        }

        public static void DeleteBook(Book book)
        {
            Books.Remove(book);
        }

        public static void DisplayBooksAll()
        {
            foreach (var book in Books)
            {
                Console.WriteLine($"ID: {book.Id} - Title: {book.Title} - Author: {book.Author} - Quantity: {book.Quantity} - Price: {book.Price}$");
            }
            Console.WriteLine();
        }

        public static void DisplayBooks(List<Book> books)
        {
            foreach (var book in books)
            {
                Console.WriteLine($"ID: {book.Id} - Title: {book.Title} - Author: {book.Author} - Quantity: {book.Quantity} - Price: {book.Price}$");
            }
            Console.WriteLine();
        }

        public static void AddLoan(Loan loan)
        {
            Loans.Add(loan);
        }

        public static void DisplayLoans()
        {
            foreach(var loan in Loans)
            {
                Console.WriteLine($"ID: {loan.Id} - Loan date: {loan.LoanDate} - Expired date: {loan.ExpireDate}");
                Console.WriteLine($"Reader: {loan.Reader.Name}");
                Console.WriteLine("Borrowed books:");
                DisplayBooks(loan.LoanBooks);
            }
        }

        public static Customer GetCustomerById(int id)
        {
            foreach(var cus in Customers)
            {
                if (cus.Id == id) return cus;
            }

            return null;
        }

        public static void DisplayCustomers()
        {
            foreach(var cus in Customers)
            {
                Console.WriteLine($"ID: {cus.Id} - Name: {cus.Name} - Address: {cus.Address}");
            }
            Console.WriteLine();
        }
    }
}
