﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_LibraryManagementSystem.Models
{
    internal class Loan
    {
        private int _id;
        public int Id { get => _id; set => _id = value; }

        private DateTime _loanDate;
        public DateTime LoanDate { get => _loanDate;set => _loanDate = value; }

        private DateTime _expireDate;
        public DateTime ExpireDate { get => _expireDate; set => _expireDate = value; }

        private Customer _reader = new Customer();
        public Customer Reader { get => _reader; set => _reader = value; } 

        private List<Book> _loanBooks = new List<Book>();
        public List<Book> LoanBooks { get => _loanBooks;set => _loanBooks = value; }

        public Loan() { }
        public Loan(int id, DateTime loanDate, DateTime expireDate, Customer reader, List<Book> loanBooks)
        {
            Id = id;
            LoanDate = LoanDate;
            ExpireDate = expireDate;
            _reader = reader;   
            _loanBooks = loanBooks;
        }
    }
}
