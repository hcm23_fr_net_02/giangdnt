﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_LibraryManagementSystem.Models
{
    internal class Book
    {
        private int _id;
        public int Id { get => _id; set => _id = value; }

        private string _title;
        public string Title { get => _title; set => _title = value; }

        private string _author;
        public string Author { get => _author; set => _author = value; }

        private int _quantity;
        public int Quantity { get => _quantity; set => _quantity = value; }

        private double _price;
        public double Price { get => _price; set => _price = value; }

        public Book() { }
        public Book(int id, string title, string author, int quantity, double price)
        {
            Id = id;
            Title = title;
            Author = author;
            Quantity = quantity;
            Price = price;
        }
    }
}
