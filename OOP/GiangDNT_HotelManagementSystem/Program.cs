﻿using GiangDNT_HotelManagementSystem.Models;
using System.Globalization;
using System.Reflection.Metadata.Ecma335;

namespace GiangDNT_HotelManagementSystem
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Hotel.Init();
            int option = 0;

            do
            {
                try
                {
                    Console.Clear();

                    MainMenu();
                    Console.Write("Choose your option: ");

                    if (!int.TryParse(Console.ReadLine(), out option))
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        continue;
                    }

                    if(option < 1 || option > 7)
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        continue;
                    }

                    switch (option)
                    {
                        case 1:
                            AddRoom();
                            break;
                        case 2:
                            DeleteRoom();
                            break;
                        case 3:
                            DisplayRooms();
                            break;
                        case 4:
                            BookingRooms();
                            break;
                        case 5:
                            DisplayBookings();
                            break;
                        case 6:
                            DisplayInvoices();
                            break;
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                    Console.ReadKey();
                }
            } while (option != 7);
        }

        static void MainMenu()
        {
            Console.WriteLine("******************************************HOTEL MANAGEMENT SYSTEM******************************************");
            Console.WriteLine("1. Add new room");
            Console.WriteLine("2. Delete room");
            Console.WriteLine("3. Display rooms");
            Console.WriteLine("4. Book rooms");
            Console.WriteLine("5. Display bookings");
            Console.WriteLine("6. Create invoice");
            Console.WriteLine("7. Exit");
            Console.WriteLine();
        }

        static void AddRoom()
        {
            Console.Clear();

            bool isvalid = true;
            int roomNumber = 0;
            int roomType = 0;
            List<string> amenities = new List<string>();

            try
            {
                do
                {
                    Console.Clear();

                    isvalid = true;

                    Console.Write("Enter room number: ");

                    if (!int.TryParse(Console.ReadLine(), out roomNumber))
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        isvalid = false;
                        continue;
                    }
                    Console.Write("Enter room type (1: Single; 2: Couple; 3: Family): ");

                    if (!int.TryParse(Console.ReadLine(), out roomType))
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        isvalid = false;
                        continue;
                    }
                    int amenOption = -1;

                    foreach (var pair in Constant.AMENITIES)
                    {
                        Console.WriteLine($"{pair.Key}. {pair.Value}");
                    }

                    do
                    {
                        Console.Write("Choose amenities for the room (Enter \"0\" for stop choosing amenties): ");
                        if (!int.TryParse(Console.ReadLine(), out amenOption))
                        {
                            Console.WriteLine("Invalid input");
                            continue;
                        }

                        if (amenOption == 0)
                        {
                            continue;
                        }
                        amenities.Add(Constant.AMENITIES[amenOption]);
                    } while (amenOption != 0);
                    var newRoom = new Room(roomNumber, Constant.ROOM_TYPES[roomType], Constant.ROOM_TYPE_PRICES[Constant.ROOM_TYPES[roomType]], amenities);
                    Hotel.AddRoom(newRoom);
                    Console.WriteLine("Add room successfully. Press any key to back to main menu");
                    Console.ReadKey();
                } while (!isvalid);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }

        static void DeleteRoom()
        {
            Console.Clear();

            bool isValid = true;
            int deletedRoomNumber = 0;
            var deletedRoom = new Room();
            Hotel.DisplayRoomsAll();

            try
            {
                do
                {
                    isValid = true;
                    Console.Write("Enter room number for deleting: ");
                    if (!int.TryParse(Console.ReadLine(), out deletedRoomNumber))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }

                    deletedRoom = Hotel.GetRoomByRoomNumber(deletedRoomNumber);

                    if (deletedRoom == null)
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                } while (!isValid);
                                
                Hotel.DeleteRoom(deletedRoom);
                Console.WriteLine("Deleted room successfully. Press any key to back to main menu");
                Console.ReadKey();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }

        static void DisplayRooms()
        {
            Console.Clear();

            try
            {
                Hotel.DisplayRoomsAllWithAmenities();
                Console.WriteLine("Press any key to back to main menu");
                Console.ReadKey();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }

        static void BookingRooms()
        {
            Console.Clear();

            bool isValid = true;
            int customerId;
            Customer customer = new Customer();
            int roomNumber;
            int roomType;
            string strRoomType;
            string strReceiveDate;
            DateTime receiveDate = new DateTime();
            List<string> specialRequirement = new List<string>();
            List<Room> bookedRooms = new List<Room>();

            Hotel.DisplayCustomers();

            try
            {
                do
                {
                    isValid = true;
                    Console.Write("Enter customer ID: ");

                    if (!int.TryParse(Console.ReadLine(), out customerId))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                    customer = Hotel.GetCustomerById(customerId);
                    if(customer == null)
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                } while (!isValid);
                Console.WriteLine();

                do
                {
                    isValid = true;
                    Console.Write("Enter room type (1: Single; 2: Couple; 3: Family): ");

                    if (!int.TryParse(Console.ReadLine(), out roomType))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                } while (!isValid);

                switch (roomType)
                {
                    case 1:
                        strRoomType = "Single";
                        break;
                    case 2:
                        strRoomType = "Couple";
                        break;
                    default:
                        strRoomType = "Family";
                        break;
                }
                Hotel.DisplayRooms(Hotel.GetRoomsByRoomType(strRoomType));
                Console.WriteLine();

                do
                {
                    Console.Write("Enter room number (Enter \"0\" to stop choosing rooms): ");

                    if (!int.TryParse(Console.ReadLine(), out roomNumber))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }

                    if(roomNumber == 0)
                    {
                        isValid = true;
                        continue;
                    }
                    var choosedRoom = Hotel.GetRoomByRoomNumber(roomNumber);
                    
                    if(choosedRoom == null)
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }

                    if (choosedRoom.Status)
                    {
                        Console.WriteLine("This room has been booked");
                        isValid = false;
                        continue;
                    }
                    else
                    {
                        isValid = false;
                        choosedRoom.Status = true;
                        Hotel.UpdateRoom(roomNumber, choosedRoom);
                        bookedRooms.Add(choosedRoom);
                    }
                } while (!isValid);

                if(bookedRooms.Count == 0)
                {
                    Console.WriteLine("You didn't choose any room yet");
                    Console.WriteLine("Press any key to back to main menu");
                    Console.ReadKey();
                    return;
                }

                do
                {
                    isValid = true;
                    Console.Write("Enter receive date (Follow this format: yyyy-mm-dd): ");

                    if (string.IsNullOrEmpty(strReceiveDate = Console.ReadLine()))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                    receiveDate = DateTime.ParseExact(strReceiveDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                } while (!isValid);

                do
                {
                    isValid = false;
                    string tempSpecRequ = "";
                    Console.Write("Enter special requirement (Enter \"stop\" to stop entering): ");

                    if (string.IsNullOrEmpty(tempSpecRequ = Console.ReadLine()))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                    specialRequirement.Add(tempSpecRequ);

                    if (tempSpecRequ.ToLower().Equals("stop"))
                    {
                        isValid = true;
                    }
                } while (!isValid);

                var booking = new Booking(DateTime.Now, receiveDate, bookedRooms, specialRequirement, customer);
                var invoice = new Invoice(0, new Dictionary<string, double>(), booking);
                Hotel.AddBooking(booking);
                Hotel.AddInvoice(invoice);
                Console.WriteLine("Book successfully. Press any key to back to main menu");
                Console.ReadKey();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }

        static void DisplayBookings()
        {
            Console.Clear();

            try
            {
                Hotel.DisplayBookings();
                Console.WriteLine("Press any key to back to main menu");
                Console.ReadKey();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }

        static void DisplayInvoices()
        {
            Console.Clear();

            bool isValid = true;
            int invoiceId = 0;
            int choosedService = 0;
            double totalPayment = 0;
            var editIvoice = new Invoice();
            Dictionary<string, double> services = new Dictionary<string, double>();

            Hotel.DisplayInvoices();

            try
            {
                do
                {
                    isValid = true;
                    Console.Write("Enter invoice ID: ");
                    if (!int.TryParse(Console.ReadLine(), out invoiceId))
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }
                    editIvoice = Hotel.GetInvoiceById(invoiceId);

                    if(editIvoice == null)
                    {
                        Console.WriteLine("Invalid input");
                        isValid = false;
                        continue;
                    }

                } while (!isValid);

                foreach (var ser in Constant.SERVICES)
                {
                    Console.WriteLine($"{ser.Key}. {ser.Value}");
                }

                do
                {
                    isValid = false;
                    Console.Write("Choose service for payment (Enter \"0\" to stop choosing service): ");

                    if (!int.TryParse(Console.ReadLine(), out choosedService))
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        isValid = false;
                        continue;
                    }

                    if(choosedService == 0)
                    {
                        isValid = true;
                        continue;
                    }

                    if(Constant.SERVICES[choosedService] == null)
                    {
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        isValid = false;
                        continue;
                    }
                    var service = Constant.SERVICES[choosedService];
                    services[service] = Constant.SERVICES_PRICE[service];
                } while (!isValid || choosedService != 0);

                int numberOfBookedDate = (DateTime.Now - editIvoice.Booking.ReceiveDate).Days;

                foreach (var ser in services.Values)
                {
                    totalPayment += ser;
                }

                foreach (var room in editIvoice.Booking.BookingRooms)
                {
                    totalPayment += room.PricePerNight * (numberOfBookedDate == 0 ? 1 : numberOfBookedDate);
                }
                editIvoice.TotalPayment = totalPayment;
                editIvoice.UsedServices = services;
                Hotel.UpdateInvoice(invoiceId, editIvoice);

                Console.Clear();
                Hotel.DisplayInvoicesWithServices();
                Console.WriteLine("Press any key to back to main menu");
                Console.ReadKey();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }
    }
}