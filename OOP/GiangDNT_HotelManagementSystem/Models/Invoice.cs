﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_HotelManagementSystem.Models
{
    internal class Invoice
    {
		private int _id;

		public int Id
		{
			get { return _id; }
			set { _id = value; }
		}

		private double _totalPayment;

		public double TotalPayment
		{
			get { return _totalPayment; }
			set { _totalPayment = value; }
		}

		private Dictionary<string, double> _usedServices = new Dictionary<string, double>();

		public Dictionary<string, double> UsedServices
		{
			get { return _usedServices; }
			set 
			{ 
				foreach(var pair in value)
				{
					_usedServices[pair.Key] = pair.Value;
				} 
			}
		}

		private Booking _booking;

		public Booking Booking
		{
			get { return _booking; }
			set { _booking = value; }
		}


		public Invoice() { }
		public Invoice(double totalPayment, Dictionary<string, double> usedServices, Booking booking)
		{
			this.Id = Hotel.invoiceIndex++;
			this.TotalPayment = totalPayment;
			this.UsedServices = usedServices;
			this.Booking = booking;
		}
	}
}
