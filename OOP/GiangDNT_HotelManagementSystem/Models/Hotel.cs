﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_HotelManagementSystem.Models
{
    internal static class Hotel
    {
        private static List<Room> Rooms = new List<Room>();
        private static List<Customer> Customers = new List<Customer>();
        private static List<Booking> Bookings = new List<Booking>();
        private static List<Invoice> Invoices = new List<Invoice>();

        public static int bookingIndex = 0;
        public static int invoiceIndex = 0;

        public static void Init()
        {
            var amenities = new List<string>()
            {
                "TV",
                "Wi-Fi",
                "Air Conditioner",
                "Dryer",
                "Shampoo",
            };
            Rooms.Add(new Room(10, "Single", 500, amenities));
            Rooms.Add(new Room(11, "Single", 500, amenities));
            Rooms.Add(new Room(12, "Couple", 1000, amenities));
            Rooms.Add(new Room(13, "Family", 1500, amenities));

            Customers.Add(new Customer(1, "Billy Juliver", "0658999487", "New York, United State"));
            Customers.Add(new Customer(2, "Emma Watson", "0324545123", "New York, United State"));
            Customers.Add(new Customer(3, "Peter Walter", "0987589154", "New York, United State"));
        }

        public static void DisplayRoomsAllWithAmenities()
        {
            foreach (var room in Rooms)
            {
                int i = 0;
                string status = "";

                if (room.Status)
                {
                    status = "Is booked";
                }
                else
                {
                    status = "Not booked";
                }
                Console.WriteLine("__________________________________________________________________________________");
                Console.WriteLine($"Room number: {room.RoomNumber} - Room type: {room.RoomType} - Price: {room.PricePerNight} - Status: {status}");
                Console.WriteLine("Room amenities:");

                foreach(var item in room.Amenities)
                {
                    Console.WriteLine($"{++i}. {item}");
                }
            }
        }

        public static void DisplayRoomsAll()
        {
            foreach (var room in Rooms)
            {
                string status = "";

                if (room.Status)
                {
                    status = "Is booked";
                }
                else
                {
                    status = "Not booked";
                }
                Console.WriteLine($"Room number: {room.RoomNumber} - Room type: {room.RoomType} - Price: {room.PricePerNight} - Status: {status}");
            }
            Console.WriteLine();
        }

        public static void DisplayRooms(List<Room> rooms)
        {
            foreach (var room in rooms)
            {
                string status = "";

                if (room.Status)
                {
                    status = "Is booked";
                }
                else
                {
                    status = "Not booked";
                }
                Console.WriteLine($"Room number: {room.RoomNumber} - Room type: {room.RoomType} - Price: {room.PricePerNight} - Status: {status}");
            }
            Console.WriteLine();
        } 

        public static void AddRoom(Room room)
        {
            Rooms.Add(room);
        }

        public static Room GetRoomByRoomNumber(int roomNumber)
        {
            foreach(var room in Rooms)
            {
                if(room.RoomNumber == roomNumber)
                {
                    return room;
                }
            }

            return null;
        }

        public static List<Room> GetRoomsByRoomType(string roomType)
        {
            return Rooms.Where(r => r.RoomType == roomType).ToList();
        }

        public static void UpdateRoom(int roomNumber, Room updatedRoom)
        {
            for (int i = 0; i < Rooms.Count; i++)
            {
                if (Rooms[i].RoomNumber == roomNumber)
                {
                    Rooms[i] = updatedRoom;
                    return;
                }
            }
        }

        public static void DeleteRoom(Room room)
        {
            Rooms.Remove(room);
        }

        public static void DisplayCustomers()
        {
            foreach(var item in Customers)
            {
                Console.WriteLine($"{item.Id}. {item.Name} - {item.PhoneNumber}");
            }
            Console.WriteLine();
        }

        public static void AddBooking(Booking booking)
        {
            Bookings.Add(booking);
        }

        public static void AddInvoice(Invoice invoice)
        {
            Invoices.Add(invoice);
        }

        public static void DisplayBookings()
        {
            int i = 0;

            foreach(var item in Bookings)
            {
                Console.WriteLine($"Booking code: {item.Id} - Customer: {item.Customer.Name} - Booking date: {item.BookingDate} - Receive date: {item.ReceiveDate}");
                Console.WriteLine("Booked rooms:");
                DisplayRooms(item.BookingRooms);
                Console.WriteLine("Special requirements:");
                foreach(var req in item.SpecialRequirements)
                {
                    Console.WriteLine($"{++i}. {req}");
                }
            }
            Console.WriteLine();
        }

        public static Customer GetCustomerById(int id)
        {
            foreach(var item in Customers)
            {
                if(item.Id == id) 
                {
                    return item;
                }
            }

            return null;
        }

        public static void DisplayInvoices()
        {
            foreach(var item in Invoices)
            {
                Console.WriteLine($"ID: {item.Id}. Total payment: {item.TotalPayment}");
            }
            Console.WriteLine();
        }

        public static void DisplayInvoicesWithServices()
        {
            int i = 0;

            foreach(var item in Invoices)
            {
                Console.WriteLine($"ID: {item.Id}. Total payment: {item.TotalPayment}");
                foreach(var pair in item.UsedServices)
                {
                    Console.WriteLine($"{++i}. {pair.Key}, Cost: {pair.Value}");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public static Invoice GetInvoiceById(int id) 
        { 
            foreach(var item in Invoices)
            {
                if(item.Id == id)
                {
                    return item;
                }
            }

            return null;
        }

        public static void UpdateInvoice(int id, Invoice updatedInvoice)
        {
            for(int i=0; i<Invoices.Count; i++)
            {
                if (Invoices[i].Id == id)
                {
                    Invoices[i] = updatedInvoice;
                    return;
                }
            }
        }
    }
}
