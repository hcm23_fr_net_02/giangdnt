﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_HotelManagementSystem.Models
{
    internal class Room
    {
		private int _roomNumber;

		public int RoomNumber
		{
			get { return _roomNumber; }
			set { _roomNumber = value; }
		}

		private string _roomType;

		public string RoomType
		{
			get { return _roomType; }
			set { _roomType = value; }
		}

		private double _pricePerNight;

		public double PricePerNight
		{
			get { return _pricePerNight; }
			set { _pricePerNight = value; }
		}

		//false -> not booked; true -> booked
		private bool _status;

		public bool Status
		{
			get { return _status; }
			set { _status = value; }
		}

		private List<string> _amenities;

		public List<string> Amenities
		{
			get { return _amenities; }
			set { _amenities = value; }
		}

		public Room() { }
		public Room(int roomNumber, string roomType, double pricePerNight, List<string> amenities)
		{
			this.RoomNumber = roomNumber;
			this.RoomType = roomType;
			this.PricePerNight = pricePerNight;
			this.Status = false;
			this.Amenities = amenities;
		}
	}
}
