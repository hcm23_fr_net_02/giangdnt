﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_HotelManagementSystem.Models
{
    internal class Booking
    {
		private int _id;
			
		public int Id
		{
			get { return _id; }
			set { _id = value; }
		}

		private DateTime _bookingDate;

		public DateTime BookingDate
		{
			get { return _bookingDate; }
			set { _bookingDate = value; }
		}

		private DateTime _receiveDate;

		public DateTime ReceiveDate
		{
			get { return _receiveDate; }
			set { _receiveDate = value; }
		}

		private List<Room> _bookingRooms;

		public List<Room> BookingRooms
		{
			get { return _bookingRooms; }
			set { _bookingRooms = value; }
		}

		private List<string> _specialRequirements;

		public List<string> SpecialRequirements
		{
			get { return _specialRequirements; }
			set { _specialRequirements = value; }
		}

		private Customer customer;

		public Customer Customer
		{
			get { return customer; }
			set { customer = value; }
		}


		public Booking() { }
		public Booking(DateTime bookingDate, DateTime receiveDate, List<Room> bookingRooms, List<string> specialRequirements, Customer customer)
		{
			this.Id = Hotel.bookingIndex++;
			this.BookingDate = bookingDate;
			this.ReceiveDate = receiveDate;
			this.BookingRooms = bookingRooms;
			this.SpecialRequirements = specialRequirements;
			this.Customer = customer;
		}
	}
}
