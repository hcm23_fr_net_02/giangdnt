﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_HotelManagementSystem
{
    internal class Constant
    {
        public static Dictionary<int, string> AMENITIES = new Dictionary<int, string>()
        {
            {1, "TV" },
            {2, "Wi-Fi" },
            {3, "Bathroom" },
            {4, "Fridge" },
            {5, "Hair Dryer" },
            {6, "Mini Bar" },
            {7, "Shampoo" },
            {8, "Air Conditioner" },
        };

        public static Dictionary<int, string> ROOM_TYPES = new Dictionary<int, string>()
        {
            {1, "Single" },
            {2, "Couple" },
            {3, "Family" },
        };

        public static Dictionary<string, double> ROOM_TYPE_PRICES = new Dictionary<string, double>()
        {
            {"Single", 500 },
            {"Couple", 1000 },
            {"Family", 1500 },
        };

        public static Dictionary<string, double> SERVICES_PRICE = new Dictionary<string, double>()
        {
            {"Dining", 100 },
            {"Drinks", 50 },
            {"Snacks", 20 },
            {"Dryer", 50 },
            {"Spa", 500 },
            {"Fitness Center", 150 },
        };

        public static Dictionary<int, string> SERVICES = new Dictionary<int, string>()
        {
            {1, "Dining" },
            {2, "Drinks" },
            {3, "Snacks" },
            {4, "Dryer" },
            {5, "Spa" },
            {6, "Fitness Center" },
        };
    }
}
