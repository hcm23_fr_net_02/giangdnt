﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_MobileStore
{
    internal class MobilePhone
    {
        private string _name;
        public string Name 
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Name cannot be null or empty");
                else
                    _name = value;
            }
        }
        private string _brand;
        public string Brand 
        {
            get => _brand;
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Brand cannot be null or empty");
                else
                    _brand = value;
            }
        }
        private int _capacityInStock;
        public int CapacityInStock 
        {
            get => _capacityInStock;
            set
            {
                if (value < 0)
                    throw new Exception("Capacity cannot smaller than 0");
                else
                    _capacityInStock = value;
            }
        }
        private double _price;
        public double Price 
        {
            get => _price;
            set
            {
                if (value < 0)
                    throw new Exception("Price cannot smaller than 0");
                else
                    _price = value;
            }
        }
        private string _code;
        public string Code 
        {
            get => _code;
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Code cannot be null or empty");
                else
                    _code = value;
            }
        }

        public MobilePhone() { }
        public MobilePhone(string name, string brand, int capacityInStock, double price, string code)
        {
            Name = name;
            Brand = brand;
            CapacityInStock = capacityInStock;
            Price = price;
            Code = code;
        }
    }
}
