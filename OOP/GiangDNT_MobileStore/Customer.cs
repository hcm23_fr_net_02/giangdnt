﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_MobileStore
{
    internal class Customer
    {
        private string _name;
        public string Name 
        {
            get => _name;
            set => _name = value;
        }
        private string _address;
        public string Address
        {
            get => _address;
            set => _address = value;
        }
        private string _code;
        public string Code
        {
            get => _code;
            set => _code = value;
        }

        public Customer() { }
        public Customer(string name, string address, string code)
        {
            Name = name;
            Address = address;
            Code = code;
        }
    }
}
