﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_MobileStore
{
    internal class Purchase
    {
        private int _identity;
        public int Identity 
        {
            get => _identity;
            set => _identity = value;
        }
        private DateTime _purchaseDate;
        public DateTime PurchaseDate 
        {
            get => _purchaseDate;
            set => _purchaseDate = value;   
        }
        public Customer Buyer { get; set; }
        public List<MobilePhone> PurchasedPhones { get; set; }

        public Purchase() { }
        public Purchase(int identity, DateTime purchaseDate, List<MobilePhone> phones, Customer customer) 
        {
            Identity = identity;
            PurchaseDate = purchaseDate;
            PurchasedPhones = new List<MobilePhone>();
            PurchasedPhones.AddRange(phones);
            Buyer = customer;
        }
    }
}
