﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_MobileStore
{
    internal static class Store
    {
        private static List<MobilePhone> mobilePhones = new List<MobilePhone>
        {
            new MobilePhone("Samsung Galaxy A12", "Samsung", 20, 5500000, "SGA12"),
            new MobilePhone("Iphone X Pro Max", "Apple", 15, 11500000, "I10PM"),
            new MobilePhone("Iphone 12 Pro Max", "Apple", 23, 19000000, "I12PM"),
            new MobilePhone("Note 10 Plus", "Samsung", 10, 15000000, "N10PL"),
            new MobilePhone("Huawei P30 Lite", "Huawei", 20, 6500000, "HP30L"),
            new MobilePhone("ROG Phone 7 Ultimate", "Asus", 50, 67900000, "ROG7U"),
            new MobilePhone("Xiaomi Redmi Note 12", "Xiaomi", 17, 5790000, "XRN12"),
            new MobilePhone("Iphone 15 Pro Max", "Apole", 17, 39990000, "I15PM"),
            new MobilePhone("Iphone 15 Plus", "Apple", 24, 34990000, "IP15P"),
            new MobilePhone("Xiaomi Pad 6", "Xiaomi", 9, 14390000, "XRN12"),
            new MobilePhone("Xiaomi Redmi A2+", "Xiaomi", 17, 2190000, "XRA2P"),
            new MobilePhone("Xiaomi Redmi 12C", "Xiaomi", 11, 2790000, "XR12C"),
            new MobilePhone("Iphone 11 Pro Max", "Apple", 8, 13000000, "I11PM"),
        };
        private static List<Customer> customers = new List<Customer>
        {
            new Customer("Nick Vujizick", "Washinton, United State", "001"),
            new Customer("Anna Watson", "Washinton, United State", "002"),
            new Customer("Jason Mammoa", "Washinton, United State", "003"),
        };
        private static List<Purchase> purchases = new List<Purchase>();

        public static int PurchaseIndex = 0;
        public static int Count 
        {
            get => mobilePhones.Count;
        }
        public static int PurchaseCount
        {
            get => purchases.Count;
        }

        public static void Add(MobilePhone phone)
        {
            mobilePhones.Add(phone);
        }

        public static MobilePhone Delete(string code)
        {
            if(mobilePhones.Count > 0)
            {
                foreach (var (item, index) in mobilePhones.Select((m, i) => (m, i)))
                {
                    if (item.Code == code)
                    {
                        mobilePhones.RemoveAt(index);
                        return item;
                    }
                }
            }

            return null;
        }

        public static void Display(List<MobilePhone> phones)
        {
            foreach(var (phone, index) in phones.Select((m, i) => (m, i)))
            {
                Console.WriteLine($"{index + 1}. {phone.Name} - Brand: {phone.Brand} - Price: {phone.Price} - In stock: {phone.CapacityInStock} - Code: {phone.Code}");
            }
            Console.WriteLine();
        }

        public static List<MobilePhone> GetAll()
        {
            return mobilePhones; 
        }

        public static MobilePhone GetByCode(string code)
        {
            foreach(var item in mobilePhones)
            {
                if (item.Code == code)
                    return item;
            }

            return null;
        }

        public static List<MobilePhone> GetPage(int pageNum, int rowOfPage = 5)
        {
            return mobilePhones.Skip((pageNum - 1) * rowOfPage).Take(rowOfPage).ToList();
        }

        public static void Purchase(Customer customer, List<MobilePhone> purchasedPhones)
        {
            purchases.Add(new Purchase(++Store.PurchaseIndex, DateTime.Now, purchasedPhones, customer));
        }

        public static void DisplayPurchases(List<Purchase> purchaseList)
        {
            foreach (var (pur, index) in purchaseList.Select((m, i) => (m, i)))
            {
                Console.WriteLine("__________________________________________________________________________________________");
                Console.WriteLine($"Buyer: {pur.Buyer.Name}, {pur.Buyer.Code}");
                Console.WriteLine($"Quantity: {pur.PurchasedPhones.Count}");
                Console.WriteLine("List of purchased phones:");
                Display(pur.PurchasedPhones);
            }
            Console.WriteLine();
        }

        public static List<Purchase> GetPagePurchases(int pageNum, int rowOfPage = 5)
        {
            return purchases.Skip((pageNum - 1) * rowOfPage).Take(rowOfPage).ToList();
        }

        public static Customer GetCustomerByCode(string code)
        {
            foreach(var item in customers)
            {
                if(item.Code == code) 
                    return item;
            }

            return null;
        }
    }
}
