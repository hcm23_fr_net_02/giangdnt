﻿using System.ComponentModel.Design;
using System.Text.RegularExpressions;

namespace GiangDNT_MobileStore
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int option = 0;
            do
            {
                try
                {
                    Console.Clear();

                    Menu();
                    Console.Write("Choose your option: ");
                    if (!int.TryParse(Console.ReadLine(), out option))
                        throw new Exception("Invalid input");
                    switch (option)
                    {
                        case 1:
                            AddPhone();
                            break;
                        case 2:
                            DeletePhone();
                            break;
                        case 3:
                            DisplayPhones();
                            break;
                        case 4:
                            PurchasePhones();
                            break;
                        case 5:
                            DisplayPurchases();
                            break;
                        case 6:
                            FindPhone();
                            break;
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine("[ERROR] {0}", ex.Message);
                }
            } while (option != 7);
        }

        static void Menu()
        {
            Console.WriteLine("*********************MOBILE STORE MENU*********************");
            Console.WriteLine("1. Add new mobile phone");
            Console.WriteLine("2. Delete mobile phone");
            Console.WriteLine("3. Display list of mobile phones");
            Console.WriteLine("4. Purchase mobile phones");
            Console.WriteLine("5. Display list of purchases");
            Console.WriteLine("6. Find mobile phone");
            Console.WriteLine("7. Exit");
            Console.WriteLine();
        }

        static void AddPhone()
        {
            Console.Clear();

            string name = "", brand = "", code = "";
            int capacity = -1;
            double price = -1;
            do
            {
                try
                {
                    Console.Write("Enter mobile phone name: ");
                    name = Console.ReadLine();
                    Console.Write("Enter mobile phone brand: ");
                    brand = Console.ReadLine();
                    Console.Write("Enter mobile phone capacity: ");
                    if (!int.TryParse(Console.ReadLine(), out capacity))
                        throw new Exception("Invalid input");
                    Console.Write("Enter mobile phone price: ");
                    if (!double.TryParse(Console.ReadLine(), out price))
                        throw new Exception("Invalid input");
                    Console.Write("Enter mobile phone code: ");
                    code = Console.ReadLine();

                    Store.Add(new MobilePhone(name, brand, capacity, price, code));
                    Console.WriteLine("Add successfully. Press any key to back to menu");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("[ERROR] {0}", ex.Message);
                    Console.WriteLine("Please enter again");
                    Console.WriteLine();
                }
            }while(string.IsNullOrEmpty(name) || string.IsNullOrEmpty(brand) || string.IsNullOrEmpty(code) || capacity < 0 || price < 0);
        }

        static void DeletePhone()
        {
            Console.Clear();

            string code = "";
            MobilePhone deletedPhone = new MobilePhone();
            do
            {
                try
                {
                    Console.Write("Enter mobile phone code (Enter \"exit\" to back to menu): ");
                    code = Console.ReadLine();
                    if (string.IsNullOrEmpty(code))
                        throw new Exception("Code cannot be empty");
                    if (code.ToLower().Equals("exit"))
                        return;
                    deletedPhone = Store.Delete(code);
                    if (deletedPhone == null)
                        throw new Exception("No mobile phone code match your input");
                    
                    Console.WriteLine($"{deletedPhone.Name} was deleted. Press any key to back to menu");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("[ERROR] {0}", ex.Message);
                    Console.WriteLine("Please enter again");
                    Console.ReadKey();
                    Console.WriteLine();
                }
            } while (string.IsNullOrEmpty(code) || deletedPhone == null);
        }

        static void DisplayPhones()
        {
            Console.Clear();

            int option = 0;
            int pageNum = 1;
            int maxPageNum = (int)Math.Ceiling(((double)Store.Count) / 5D);
            if (maxPageNum > 0)
            {
                do
                {
                    try
                    {
                        Console.Clear();
                        Console.WriteLine("*********************MOBILE PHONES LIST*********************");
                        Console.WriteLine($"[Page {pageNum}/{maxPageNum}]");
                        Store.Display(Store.GetPage(pageNum));
                        DisplayMenu();
                        Console.Write("Choose your option: ");
                        if (!int.TryParse(Console.ReadLine(), out option))
                            throw new Exception("Invalid input");
                        switch (option)
                        {
                            case 1:
                                if (pageNum == 1)
                                {
                                    Console.WriteLine("Reach first page. Press any key to continue");
                                    Console.ReadKey();
                                    break;
                                }
                                pageNum--;
                                break;
                            case 2:
                                if (pageNum == maxPageNum)
                                {
                                    Console.WriteLine("Reach final page. Press any key to continue");
                                    Console.ReadKey();
                                    break;
                                }
                                pageNum++;
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("[ERROR] {0}", ex.Message);
                        Console.WriteLine("Please enter again");
                        Console.ReadKey();
                    }
                } while (option != 3);
            }
            else
            {
                Console.WriteLine("No phones added");
                Console.ReadKey();
            }

            void DisplayMenu()
            {
                Console.WriteLine("1. Previous page");
                Console.WriteLine("2. Next page");
                Console.WriteLine("3. Exit to menu");
            }
        }

        static void PurchasePhones()
        {
            Console.Clear();

            string customerCode = "";
            Customer buyer = new Customer();
            do
            {
                try
                {
                    Console.Write("Enter customer code (Enter \"exit\" to back to menu): ");
                    customerCode = Console.ReadLine();

                    if (string.IsNullOrEmpty(customerCode))
                        throw new Exception("Code cannot be empty");

                    if (customerCode.ToLower().Equals("exit"))
                        return;

                    buyer = Store.GetCustomerByCode(customerCode);

                    if (buyer == null)
                        throw new Exception("No customer match your code");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("[ERROR] {0}", ex.Message);
                    Console.WriteLine("Please enter again");
                    Console.ReadKey();
                    Console.WriteLine();
                }
            } while (string.IsNullOrEmpty(customerCode) || buyer == null);

            int option = 0;
            string phoneCode = "";
            List<MobilePhone> purchasedPhones = new List<MobilePhone>();
            do
            {
                try
                {
                    Console.Clear();
                    Store.Display(Store.GetAll());
                    Console.Write("Enter mobile phone code: ");
                    phoneCode = Console.ReadLine();
                    if (string.IsNullOrEmpty(phoneCode))
                        throw new Exception("Code cannot be empty");
                    var phone = Store.GetByCode(phoneCode);
                    if (phone == null)
                        throw new Exception("No phone match your code");
                    if (phone.CapacityInStock == 0)
                        throw new Exception("Out of stock");
                    purchasedPhones.Add(phone);

                    PurchaseMenu();
                    Console.Write("Choose your option: ");
                    if (!int.TryParse(Console.ReadLine(), out option))
                        throw new Exception("Invalid input");
                    if (option != 1 && option != 2)
                        throw new Exception("Invalid input");
                    if(option == 2)
                        Store.Purchase(buyer, purchasedPhones);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("[ERROR] {0}", ex.Message);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }
            } while (option != 2);

            void PurchaseMenu()
            {
                Console.WriteLine("1. Continue purchase");
                Console.WriteLine("2. Exit to menu");
            }
        }

        static void DisplayPurchases()
        {
            Console.Clear();

            int option = 0;
            int pageNum = 1;
            int maxPageNum = (int)Math.Ceiling(((double)Store.PurchaseCount) / 5D);
            
            if(maxPageNum > 0)
            {
                do
                {
                    try
                    {
                        Console.Clear();
                        Console.WriteLine("*********************PURCHASED LIST*********************");
                        Console.WriteLine($"[Page {pageNum}/{maxPageNum}]");
                        Store.DisplayPurchases(Store.GetPagePurchases(pageNum));
                        DisplayMenu();
                        Console.Write("Choose your option: ");
                        if (!int.TryParse(Console.ReadLine(), out option))
                            throw new Exception("Invalid input");
                        switch (option)
                        {
                            case 1:
                                if (pageNum == 1)
                                {
                                    Console.WriteLine("Reach first page. Press any key to continue");
                                    Console.ReadKey();
                                    break;
                                }
                                pageNum--;
                                break;
                            case 2:
                                if (pageNum == maxPageNum)
                                {
                                    Console.WriteLine("Reach final page. Press any key to continue");
                                    Console.ReadKey();
                                    break;
                                }
                                pageNum++;
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("[ERROR] {0}", ex.Message);
                        Console.WriteLine("Please enter again");
                        Console.ReadKey();
                        Console.WriteLine(); ;
                    }
                } while (option != 3);
            }
            else
            {
                Console.WriteLine("No one made purchase");
                Console.ReadKey();
            }

            void DisplayMenu()
            {
                Console.WriteLine("1. Previous page");
                Console.WriteLine("2. Next page");
                Console.WriteLine("3. Exit to menu");
            }

        }

        static void FindPhone()
        {
            Console.Clear();

            string phoneCode = "";
            do
            {
                try
                {
                    Console.Write("Enter phone code: ");
                    phoneCode = Console.ReadLine();
                    if (string.IsNullOrEmpty(phoneCode))
                        throw new Exception("Code cannot be empty");
                    var phone = Store.GetByCode(phoneCode);
                    if (phone == null)
                        throw new Exception("No phone match your code");
                    Store.Display(new List<MobilePhone> { phone });
                    Console.WriteLine("Press any key to back to menu");
                    Console.ReadKey();
                }
                catch(Exception ex)
                {
                    Console.WriteLine("[ERROR] {0}", ex.Message);
                    Console.WriteLine("Please enter again");
                    Console.ReadKey();
                }
            } while (string.IsNullOrEmpty(phoneCode) || phoneCode == null);
        }
    }
}