﻿using GiangDNT_MiniProject_LibraryManagementSystem.Models;
using GiangDNT_MiniProject_LibraryManagementSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_MiniProject_LibraryManagementSystem
{
    [TestClass]
    public class ReviewRepositoryTests
    {
        UnitOfWork uow = new UnitOfWork();

        [TestMethod]
        public void GetValue_IdIsNotExist_ReturnNull()
        {
            //assert            
            int param = 100;

            //act
            Review actual = uow.ReviewRepository.GetValue(param);

            //arrange
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void GetValue_IdIsExist_ReturnOneReview()
        {
            //assert            
            int param = 1;

            //act
            Review actual = uow.ReviewRepository.GetValue(param);

            //arrange
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void Update_UpdateReviewIsNotExist_DoNotUpdate()
        {
            //assert
            Review param = new Review(100, "test", "test", 1, 1, 1);

            //act
            uow.ReviewRepository.Update(param);

            //arrange
            Assert.AreNotEqual(param, uow.ReviewRepository.GetValue(100));
        }

        [TestMethod]
        public void Update_UpdateReviewIsExist_UpdateMatchingReview()
        {
            //assert
            var param = uow.ReviewRepository.GetValue(1);
            param.Title = "test title";
            param.Content = "test content";

            //act
            uow.ReviewRepository.Update(param);

            //arrange
            Assert.AreEqual(param, uow.ReviewRepository.GetValue(1));
        }

        [TestMethod]
        public void GetLastId_ListIsNotEmpty_ReturnLastIdInTheList()
        {
            //assert
            var expected = 2;

            //act
            var actual = uow.ReviewRepository.GetLastId();

            //arrange
            Assert.AreEqual(expected, actual);
        }
    }
}
