﻿using GiangDNT_MiniProject_LibraryManagementSystem;
using GiangDNT_MiniProject_LibraryManagementSystem.Models;

namespace Test_MiniProject_LibraryManagementSystem
{
    [TestClass]
    public class ReaderRepositoryTests
    {
        UnitOfWork uow = new UnitOfWork();

        [TestMethod]
        public void GetValue_IdIsNotExist_ReturnNull()
        {
            //assert            
            int param = 100;

            //act
            Reader actual = uow.ReaderRepository.GetValue(param);

            //arrange
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void GetValue_IdIsExist_ReturnOneReader()
        {
            //assert            
            int param = 1;

            //act
            Reader actual = uow.ReaderRepository.GetValue(param);

            //arrange
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void Update_UpdateReaderIsNotExist_DoNotUpdate()
        {
            //assert
            Reader param = new Reader(100, "Test test", 100, DateTime.Now);

            //act
            uow.ReaderRepository.Update(param);

            //arrange
            Assert.AreNotEqual(param, uow.ReaderRepository.GetValue(100));
        }

        [TestMethod]
        public void Update_UpdateReaderIsExist_UpdateMatchingReader()
        {
            //assert
            var param = uow.ReaderRepository.GetValue(1);
            param.FullName = "test update";
            param.Age = 99;

            //act
            uow.ReaderRepository.Update(param);

            //arrange
            Assert.AreEqual(param, uow.ReaderRepository.GetValue(1));
        }

        [TestMethod]
        public void GetLastId_ListIsNotEmpty_ReturnLastIdInTheList()
        {
            //assert
            var expected = 11;

            //act
            var actual = uow.ReaderRepository.GetLastId();

            //arrange
            Assert.AreEqual(expected, actual);
        }
    }
}
