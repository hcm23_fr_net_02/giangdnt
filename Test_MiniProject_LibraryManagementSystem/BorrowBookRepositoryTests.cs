﻿using GiangDNT_MiniProject_LibraryManagementSystem.Models;
using GiangDNT_MiniProject_LibraryManagementSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_MiniProject_LibraryManagementSystem
{
    [TestClass]
    public class UnitTest_BorrowBorrowBorrowBookRepository
    {
        UnitOfWork uow = new UnitOfWork();

        [TestMethod]
        public void GetValue_IdIsNotExist_ReturnNull()
        {
            //assert            
            int param = 100;

            //act
            BorrowBook actual = uow.BorrowBookRepository.GetValue(param);

            //arrange
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void GetValue_IdIsExist_ReturnOneBorrowBook()
        {
            //assert            
            int param = 1;

            //act
            BorrowBook actual = uow.BorrowBookRepository.GetValue(param);

            //arrange
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void Update_UpdateBorrowBookIsNotExist_DoNotUpdate()
        {
            //assert
            BorrowBook param = new BorrowBook(100, DateTime.Now, DateTime.Now.AddDays(2), 1, 1);

            //act
            uow.BorrowBookRepository.Update(param);

            //arrange
            Assert.AreNotEqual(param, uow.BorrowBookRepository.GetValue(100));
        }

        [TestMethod]
        public void Update_UpdateBorrowBookIsExist_UpdateMatchingBorrowBook()
        {
            //assert
            var param = uow.BorrowBookRepository.GetValue(1);
            param.BookId = 2;
            param.ReaderId = 2;

            //act
            uow.BorrowBookRepository.Update(param);

            //arrange
            Assert.AreEqual(param, uow.BorrowBookRepository.GetValue(1));
        }

        [TestMethod]
        public void GetLastId_ListIsNotEmpty_ReturnLastIdInTheList()
        {
            //assert
            var expected = 2;

            //act
            var actual = uow.BorrowBookRepository.GetLastId();

            //arrange
            Assert.AreEqual(expected, actual);
        }
    }
}
