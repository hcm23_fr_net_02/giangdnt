using GiangDNT_MiniProject_LibraryManagementSystem;
using GiangDNT_MiniProject_LibraryManagementSystem.Models;

namespace Test_MiniProject_LibraryManagementSystem
{
    [TestClass]
    public class BookRepositoryTests
    {
        UnitOfWork uow = new UnitOfWork();

        [TestMethod]
        public void GetValue_IdIsNotExist_ReturnNull()
        {
            //assert            
            int param = 100;

            //act
            Book actual = uow.BookRepository.GetValue(param);

            //arrange
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void GetValue_IdIsExist_ReturnOneBook()
        {
            //assert            
            int param = 1;

            //act
            Book actual = uow.BookRepository.GetValue(param);

            //arrange
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void GetAll_ReturnListOfBooks()
        {
            //assert
            int expected = uow.BookRepository.Count; 

            //act
            int actual = uow.BookRepository.GetAll().Count;

            //arrange
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Find_PredicateIsNull_ReturnAllBooks()
        {
            //assert
            Func<Book, bool> predicate = null;

            //act
            List<Book> actual = uow.BookRepository.Find(predicate);

            //arrange
            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.Count, uow.BookRepository.Count);
        }

        [TestMethod]
        public void Find_PredicateIsNotNull_ReturnMatchedBooks()
        {
            //assert
            Func<Book, bool> predicate = b => b.Title == "Golden Age";

            //act
            List<Book> actual = uow.BookRepository.Find(predicate);

            //arrange
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void Add_InsertBookIsNull_NothingAddToList()
        {
            //assert
            Book param = null;
            var expected = uow.BookRepository.Count;

            //act
            uow.BookRepository.Add(param);
            var actual = uow.BookRepository.Count;

            //arrange
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Add_InsertBookIsNotNull_AddOneBookToList()
        {
            //assert
            var param = new Book(uow.BookRepository.GetLastId() + 1, "test", "test", "2000", 20, "test");
            var expected = uow.BookRepository.Count + 1;

            //act
            uow.BookRepository.Add(param);
            var actual = uow.BookRepository.Count;

            //arrange
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Update_UpdateBookIsNotExist_DoNotUpdate()
        {
            //assert
            Book param = new Book(100, "test", "test", "2000", 4, "test");

            //act
            uow.BookRepository.Update(param);

            //arrange
            Assert.AreNotEqual(param, uow.BookRepository.GetValue(100));
        }

        [TestMethod]
        public void Update_UpdateBookIsExist_UpdateMatchingBook()
        {
            //assert
            var param = uow.BookRepository.GetValue(1);
            param.Title = "test";
            param.Author = "test";
            param.Genre = "test";

            //act
            uow.BookRepository.Update(param);

            //arrange
            Assert.AreEqual(param, uow.BookRepository.GetValue(1));
        }

        [TestMethod]
        public void Remove_BookIsNotExist_NoBookIsRemoved()
        {
            //assert
            Book param = new Book(100, "test", "test", "2000", 4, "test");
            var expected = uow.BookRepository.Count;

            //act
            uow.BookRepository.Remove(param);
            var actual = uow.BookRepository.Count;

            //arrange
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Remove_BookIsExist_RemoveOneBook()
        {
            //assert
            var param = uow.BookRepository.GetValue(1);
            var expected = uow.BookRepository.Count - 1;

            //act
            uow.BookRepository.Remove(param);
            var actual = uow.BookRepository.Count;

            //arrange
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetLastId_ListIsNotEmpty_ReturnLastIdInTheList()
        {
            //assert
            var expected = 23;

            //act
            var actual = uow.BookRepository.GetLastId();

            //arrange
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod]
        //public void Filter_KeySelectorIsNotExist_ReturnEmptyList()
        //{
        //    //assert

        //    //act
        //    var actual = uow.BookRepository.Filter(x => "a@_");

        //    //arrange
        //    Assert.AreEqual(actual.ToList().Count, 0);
        //}

        [TestMethod]
        public void Filter_KeySelectorIsExist_ReturnListOfIGrouping()
        {
            //assert

            //act
            var actual = uow.BookRepository.Filter(x => x.Genre);

            //arrange
            Assert.IsNotNull(actual);
        }
    }
}