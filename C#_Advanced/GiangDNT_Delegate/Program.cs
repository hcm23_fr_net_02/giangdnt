﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_Delegate
{
    class Program
    {
        static void Main(string[] args)
        {
            DelegatePractice dp = new DelegatePractice();
            Console.WriteLine(dp.PerformOperation<double>(7.124, 3.764, dp.add_double));
            Console.WriteLine(dp.PerformOperation<float>(24.34F, 13.333F, dp.add_float));
            Console.WriteLine(dp.PerformOperation<string>("hello ", "world", dp.add_string));
            Console.WriteLine(dp.PerformOperation<Student>(new Student { ID = 1, Name = "Bill", Score = 6.75 }, new Student { ID = 2, Name = "John", Score = 1.5 }, dp.add_struct));
            Console.ReadKey();
        }
    }

    public struct Student
    {
        public int ID;
        public string Name;
        public double Score;
        public override string ToString()
        {
            return string.Format("ID: {0}, Name: {1}, Score: {2}",ID,Name,Score);
        }
    }

    class DelegatePractice
    {
        public delegate T MathOperation<T>(T param1, T param2);
        public MathOperation<float> add_float = (num1, num2) => num1 + num2;
        public MathOperation<double> add_double = (num1, num2) => num1 + num2;
        public MathOperation<string> add_string = (str1, str2) => str1 + str2;
        public MathOperation<Student> add_struct = (stu1, stu2) =>
        {
            return new Student { ID = 3, Name = "Peter", Score = stu1.Score + stu2.Score };
        };

        public T PerformOperation<T>(T param1, T param2, MathOperation<T> operation)
        {
            return operation(param1, param2);
        }
    }
}
