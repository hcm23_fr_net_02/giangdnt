﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace GiangDNT_Async
{
    class Program
    {
        public static HttpClient httpClient = new HttpClient();
        public static string url = "https://651e54b644a3a8aa47681f47.mockapi.io/todos/";
        public static JsonSerializerOptions options = new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true
        };

        static async Task Main(string[] args)
        {                           
            int option;

            do
            {
                Console.Clear();
                new Program().Menu();
                option = int.Parse(Console.ReadLine());
                switch (option)
                {
                    case 1:
                        Console.WriteLine("1. Order by created date");
                        Console.WriteLine("2. Order by priority");
                        Console.Write("Please choose your option: ");
                        var orderBy = int.Parse(Console.ReadLine());                        
                        await ToDoList.Display(orderBy);
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        break;
                    case 2:
                        Console.Write("Enter description: ");
                        string des = Console.ReadLine();
                        Console.Write("Enter priority: ");
                        int prior = int.Parse(Console.ReadLine());
                        await ToDoList.Add(new ToDo(des,DateTime.Now,prior));
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        break;
                    case 3:
                        await ToDoList.Display(1);
                        Console.WriteLine();
                        Console.Write("Please enter the ID of the Todo that you want to delete: ");
                        var id = Console.ReadLine();
                        await ToDoList.Delete(id);
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        break;
                    case 4:
                        await ToDoList.Display(1);
                        Console.WriteLine();
                        Console.Write("Please enter the ID of the Todo that you want to check as Completed: ");
                        id = Console.ReadLine();
                        await ToDoList.Completed(id);
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        break;
                    case 5:
                        await ToDoList.Display(2);
                        Console.WriteLine();
                        Console.Write("Please enter the ID of the Todo that you want to check as Completed: ");
                        id = Console.ReadLine();
                        Console.Write("Enter priority: ");
                        prior = int.Parse(Console.ReadLine());
                        await ToDoList.RaisePriority(id, prior);
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        break;
                }
            } while (option != 6);
        }  
        
        void Menu()
        {
            Console.WriteLine("==========TODO LIST==========");
            Console.WriteLine("1. View todo list");
            Console.WriteLine("2. Add todo");
            Console.WriteLine("3. Delete todo");
            Console.WriteLine("4. Check todo as completed");
            Console.WriteLine("5. Raise todo priority");
            Console.WriteLine("6. Exit");
            Console.WriteLine();
            Console.Write("Please choose your option: ");
        }
    }

    class ToDo
    {
        public string ID { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Priority { get; set; }
        public bool IsCompleted { get; set; }

        public ToDo() { }
        public ToDo(string description, DateTime createdDate, int priority, bool isCompleted = false)
        {
            Description = description;
            CreatedDate = createdDate;
            Priority = priority;
            IsCompleted = isCompleted;
        }


    }

    static class ToDoList
    {
        public static async Task Add(ToDo todo)
        {
            var stringContent = new StringContent(JsonSerializer.Serialize(todo), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await Program.httpClient.PostAsync(Program.url, stringContent);
            //Console.WriteLine(await response.Content.ReadAsStringAsync());
            if(!response.IsSuccessStatusCode)
                Console.WriteLine("[{0}] Add failed", response.StatusCode);
        }

        public static async Task Delete(string id)
        {
            HttpResponseMessage response = await Program.httpClient.DeleteAsync(Program.url + id);
            if (!response.IsSuccessStatusCode)
                Console.WriteLine("[{0}] Delete failed", response.StatusCode);
        }

        public static async Task Completed(string id)
        {
            HttpResponseMessage response = await Program.httpClient.GetAsync(Program.url + id);
            if (!response.IsSuccessStatusCode)
                Console.WriteLine("[{0}] Delete failed", response.StatusCode);
            string content = await response.Content.ReadAsStringAsync();
            ToDo todo = JsonSerializer.Deserialize<ToDo>(content, Program.options);
            todo.IsCompleted = true;
            var stringContent = new StringContent(JsonSerializer.Serialize(todo), Encoding.UTF8, "application/json");
            response = await Program.httpClient.PutAsync(Program.url + id, stringContent);
            if (!response.IsSuccessStatusCode)
                Console.WriteLine("[{0}] Delete failed", response.StatusCode);
        }

        public static async Task RaisePriority(string id, int priority)
        {
            HttpResponseMessage response = await Program.httpClient.GetAsync(Program.url + id);
            if (!response.IsSuccessStatusCode)
                Console.WriteLine("[{0}] Delete failed", response.StatusCode);
            string content = await response.Content.ReadAsStringAsync();
            ToDo todo = JsonSerializer.Deserialize<ToDo>(content, Program.options);
            todo.Priority = priority;
            var stringContent = new StringContent(JsonSerializer.Serialize(todo), Encoding.UTF8, "application/json");
            response = await Program.httpClient.PutAsync(Program.url + id, stringContent);
            if (!response.IsSuccessStatusCode)
                Console.WriteLine("[{0}] Delete failed", response.StatusCode);
        }

        public static async Task Display(int orderBy) //1->by created date; 2->by priority
        {
            HttpResponseMessage response = await Program.httpClient.GetAsync(Program.url);
            if (!response.IsSuccessStatusCode)
                Console.WriteLine("[{0}] Something went wrong", response.StatusCode);
            string content = await response.Content.ReadAsStringAsync();                        
            List<ToDo> todos = JsonSerializer.Deserialize<List<ToDo>>(content, Program.options);
            if (todos.Count == 0)
            {
                Console.WriteLine("You haven't added any todo");
                return;
            }
            if(orderBy == 1)
            {
                todos = todos.OrderBy(todo => todo.CreatedDate).ToList();
            }
            else
            {
                todos = todos.OrderByDescending(todo => todo.Priority).ToList();
            }
           
            Console.WriteLine("==========YOUR TODO LIST==========");
            for (int i = 0; i < todos.Count; i++)
            {
                Console.WriteLine("{0}. [{1} - Create at {2} - Priority: {3}] {4}", todos[i].ID, todos[i].IsCompleted ? "Completed" : "Not Completed", todos[i].CreatedDate, todos[i].Priority, todos[i].Description);
            }
        }
    }
}
