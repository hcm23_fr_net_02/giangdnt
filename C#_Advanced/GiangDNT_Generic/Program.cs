﻿using System;

namespace GiangDNT_Generic
{
    class Program
    {
        static void Main(string[] args)
        {
            //DataPair<int, string> dp1 = new DataPair<int, string>(5, "Hello",3);
            //Console.WriteLine(dp1.Data1);
            //Console.WriteLine(dp1.Data2);
            //DataPair<double, bool> dp2 = new DataPair<double, bool>(5.35332, false,5);
            //Console.WriteLine(dp2.Data1);
            //Console.WriteLine(dp2.Data2);

            //A a = new A();
            //Console.WriteLine(a.GetLastItem<int>(new int[] { 1, 2, 3, 5, 4 }));
            //Console.WriteLine(a.GetLastItem<string>(new string[] { "one", "two", "three", "five", "four" }));

            //Stack<Person> stack = new Stack<Person>(5);
            //stack.Push(new Person("John", "09834324"));
            //stack.Push(new Person("Mary", "09834324"));
            //stack.Push(new Person("Peter", "09834324"));
            //stack.Push(new Person("Josh", "09834324"));
            //stack.Push(new Person("Bob", "09834324"));
            //Console.WriteLine(stack.Peak().Name);
            //Console.WriteLine(stack.Pop().Name);
            //Console.WriteLine(stack.Pop().Name);
            //Console.WriteLine(stack.Peak().Name);

            Stack<Student> stack = new Stack<Student>(5);
            Student stu1;
            stu1.ID = "001";
            stu1.Name = "Pickle";
            Student stu2;
            stu2.ID = "002";
            stu2.Name = "Pablo";
            Student stu3;
            stu3.ID = "003";
            stu3.Name = "Murphy";
            stack.Push(stu1);
            stack.Push(stu2);
            stack.Push(stu3);
            Console.WriteLine(stack.Peak().Name);
            Console.WriteLine(stack.Pop().Name);
            Console.WriteLine(stack.Pop().Name);
            Console.WriteLine(stack.Peak().Name);

            Console.ReadKey();
        }
    }

    class Person
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }

        public Person() { }
        public Person(string name, string phoneNumber)
        {
            Name = name;
            PhoneNumber = phoneNumber;
        }
    }

    struct Student
    {
        public string ID;
        public string Name;
    }

    public class Stack<T>
    {
        private T[] items;
        private int lastItemIndex;
        public bool IsEmpty { get { return lastItemIndex == -1; } }
        private int count;
        public int Count
        {
            get
            {
                return count;
            }
        }

        public Stack()
        {
            items = new T[10];
            count = 0;
            lastItemIndex = -1;
        }

        public Stack(int size)
        {
            items = new T[size];
            count = 0;
            lastItemIndex = -1;
        }

        public void Push(T item)
        {
            if (count < items.Length)
            {
                count++;
                items[++lastItemIndex] = item;

                return;
            }
            else
                throw new InvalidOperationException("Stack reached max size");
        }

        public T Pop()
        {
            if (!this.IsEmpty)
            {
                count--;

                return items[lastItemIndex--];
            }

            throw new InvalidOperationException("Stack is empty");
        }

        public T Peak()
        {
            if (!this.IsEmpty)
                return items[lastItemIndex];

            throw new InvalidOperationException("Stack is empty");
        }

        public void Clear()
        {
            try
            {
                for(int i = 0; i <= lastItemIndex; i++)
                {
                    items[i] = default(T);
                }
                lastItemIndex = -1;
                count = 0;
            }
            catch(Exception e)
            {
                Console.WriteLine("Exception: {0}", e);
                throw new InvalidOperationException("Something went wrong");
            }
        }
    }


    public class DataPair<T, U>
    {
        public T Data1 { get; set; }
        public U Data2 { get; set; }

        public DataPair(T data1, U data2, int data3)
        {
            Data1 = data1;
            Data2 = data2;
        }
    }

    public class A
    {
        public T GetLastItem<T>(T[] array)
        {
            return array[array.Length-1];
        }
    }
}
