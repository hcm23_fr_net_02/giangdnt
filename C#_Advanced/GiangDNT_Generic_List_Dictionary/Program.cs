﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiangDNT_Generic_List_Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            ManageStudentMark stu = new ManageStudentMark();
            int choice;
            do
            {
                Console.Clear();
                stu.Menu();
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        int subChoice;
                        string name;
                        double mark;
                        do
                        {
                            do
                            {
                                Console.Write("Enter student's name: ");
                                name = Console.ReadLine();
                                if (name == "")
                                {
                                    Console.WriteLine("Please enter a valid name");
                                }
                            } while (name == "");
                            do
                            {
                                Console.Write("Enter student's mark: ");
                                mark = double.Parse(Console.ReadLine());
                                if (mark < 0 && mark > 10)
                                {
                                    Console.WriteLine("Please enter a valid mark");
                                }
                            } while (mark < 0 && mark > 10);
                            stu.Add(name, mark);
                            Console.WriteLine("Press \"4\" to back to menu or press any number to continue...");
                            subChoice = int.Parse(Console.ReadLine());
                        } while (subChoice != 4);
                        break;
                    case 2:
                        Console.Clear();
                        int subChoice1;
                        do
                        {
                            Console.Write("Please enter student's name: ");
                            string name1 = Console.ReadLine();
                            double mark1 = stu.GetStudentMarkByName(name1);
                            if (mark1 != -1)
                            {
                                Console.WriteLine("Mark: {0}", mark1);
                            }
                            Console.WriteLine("Press \"4\" to back to menu or press any number to continue...");
                            subChoice1 = int.Parse(Console.ReadLine());
                        } while (subChoice1!=4);
                        break;
                    case 3:
                        Console.Clear();
                        stu.DisplayStudents();
                        Console.WriteLine("Press any key to back to menu");
                        Console.ReadKey();
                        break;
                }
            } while (choice != 4);
        }
    }

    class ManageStudentMark
    {
        public Dictionary<string, double> students = new Dictionary<string, double>();

        public void Add(string name, double mark)
        {
            students.Add(name, mark);
        }
        public double GetStudentMarkByName(string name)
        {
            if (students.ContainsKey(name))
            {
                double result;
                if(students.TryGetValue(name, out result))
                    return result;
            }

            Console.WriteLine("Student's name doesn't exist");
            return -1;
        }
        public void DisplayStudents()
        {
            foreach(var student in students)
            {
                Console.WriteLine("Name: {0}, score: {1}", student.Key, student.Value);
            }
        }
        public void Menu()
        {
            Console.WriteLine("============ Student Mark Management System ============");
            Console.WriteLine("1. Add student");
            Console.WriteLine("2. View mark by student's name");
            Console.WriteLine("3. Display all students");
            Console.WriteLine("4. Exit");
            Console.WriteLine();
            Console.Write("Please choose your option: ");
        }
    }
    
}
