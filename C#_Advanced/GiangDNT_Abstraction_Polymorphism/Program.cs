﻿using System.Threading.Channels;

namespace GiangDNT_Abstraction_Polymorphism
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IShape[] shapes = new IShape[9];
            IShape r1 = new Rectangle(6.4, 5.5);
            IShape r2 = new Rectangle(20, 17);
            IShape r3 = new Rectangle(1.5, 0.5);
            IShape t1 = new Triangle(1, 2.5, 3);
            IShape t2 = new Triangle(6, 9, 10);
            IShape t3 = new Triangle(3.6, 10.5, 9);
            IShape c1 = new Circle(8.9);
            IShape c2 = new Circle(5);
            IShape c3 = new Circle(3.14);
            shapes[0] = r1;
            shapes[1] = r2;
            shapes[2] = r3;
            shapes[3] = t1;
            shapes[4] = t2;
            shapes[5] = t3;
            shapes[6] = c1;
            shapes[7] = c2;
            shapes[8] = c3;

            foreach(var (value,index) in shapes.Select((s,i)=> (s,i)))
            {
                Console.WriteLine($"[{index}] Type: {value.GetType()}, Area: {value.CalculateArea()}, Perimeter: {value.CalculatePerimeter()}");
            }
        }
    }

    public interface IShape
    {
        double CalculateArea();
        double CalculatePerimeter();
    }

    public class Rectangle : IShape
    {
        public double LongEdge { get; set; }
        public double ShortEdge { get; set; }
        public Rectangle() { }
        public Rectangle(double longEdge, double shortEdge)
        {
            LongEdge = longEdge;
            ShortEdge= shortEdge; 
        }
        public double CalculateArea()
        {
            return Math.Round(LongEdge * ShortEdge, 2);
        }
        public double CalculatePerimeter()
        {
            return Math.Round(2 * (ShortEdge + LongEdge), 2);
        }
    }

    public class Triangle : IShape
    {
        public double Edge1 { get; set; }
        public double Edge2 { get; set; }
        public double Edge3 { get; set; }
        public Triangle() { }
        public Triangle(double edge1, double edge2, double edge3)
        {
            Edge1 = edge1;
            Edge2 = edge2;
            Edge3 = edge3;
        }
        public double CalculateArea()
        {
            double s = (Edge1 + Edge2 + Edge3) / 2;
            return Math.Round(Math.Sqrt(s * (s - Edge1) * (s - Edge2) * (s - Edge3)), 2);
        }
        public double CalculatePerimeter()
        {
            return Math.Round(Edge1 + Edge2 + Edge3, 2);
        }
    }

    public class Circle : IShape
    {
        public double Radius { get; set; }
        public Circle() { }
        public Circle(double radius)
        {
            Radius = radius;
        }
        public double CalculateArea()
        {
            return Math.Round(Math.PI * Radius * Radius, 2);
        }
        public double CalculatePerimeter()
        {
            return Math.Round(2 * Math.PI * Radius, 2);
        }
    }
}