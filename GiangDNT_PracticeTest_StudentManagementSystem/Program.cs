﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GiangDNT_StudentManagementSystem
{
    class Program
    {        
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            int option;
            do
            {
                Console.Clear();
                Menu();
                option = int.Parse(Console.ReadLine());
                switch (option)
                {
                    case 1:
                        AddStudent();
                        break;
                    case 2:
                        ViewStudent();
                        break;
                    case 3:
                        FindStudent();
                        break;
                    case 4:
                        DeleteSudent();
                        break;
                    default:
                        break;
                }
            } while (option != 5);
        }

        static void Menu()
        {
            Console.WriteLine("**************Main Menu**************");
            Console.WriteLine("1. Add Student");
            Console.WriteLine("2. View Student list");
            Console.WriteLine("3. Find Student");
            Console.WriteLine("4. Delete Student");
            Console.WriteLine("5. Exit");

            Console.Write("Please choose your option: ");
        }

        static void AddStudent()
        {
            Console.Clear();

            string name = "";
            int age = 0;
            double gpa = -1;
            string input = "";

            do
            {
                Console.Write("Enter student name: ");
                name = Console.ReadLine();
                if(string.IsNullOrEmpty(name))
                    Console.WriteLine("Invalid name, please enter again");
            } while (string.IsNullOrEmpty(name));
            do
            {
                Console.Write("Enter student age: ");
                input = Console.ReadLine();
                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("Invalid age, please enter again");
                    continue;
                }
                age = int.Parse(input);
                if (age <= 0)
                    Console.WriteLine("Invalid age, please enter again");
            } while (age <= 0);
            do
            {
                Console.Write("Enter student GPA: ");
                input = Console.ReadLine();
                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("Invalid GPA, please enter again");
                    continue;
                }
                gpa = double.Parse(input);
                if (gpa < 0 || gpa > 10)
                    Console.WriteLine("Invalid GPA, please enter again");
            } while (gpa < 0 || gpa > 10);
            StudentList.Add(new Student(++StudentList.Index, name, age, gpa));

            Console.WriteLine("Pres any key to continue...");
            Console.ReadKey();
        }
        static void ViewStudent()
        {
            Console.Clear();

            int option;
            int pageNum = 1;
            int maxPageNum = (int)Math.Ceiling((double)StudentList.Count / 5D);
            StudentList.View(5, pageNum);
            do
            {
                ViewMenu();
                do
                {
                    Console.Write("Choose your option: ");
                    option = int.Parse(Console.ReadLine());
                    if (option != 1 && option != 2 && option != 3)
                        Console.WriteLine("Invalid option, please enter again");
                } while (option != 1 && option != 2 && option != 3);
                switch (option)
                {
                    case 1:
                        if (pageNum == 1)
                        {
                            Console.WriteLine("Reach first page");
                        }
                        else
                        {
                            StudentList.View(5, --pageNum);
                        }
                        break;
                    case 2:
                        if (pageNum == maxPageNum)
                        {
                            Console.WriteLine("Reach final page");
                        }
                        else
                        {
                            StudentList.View(5, ++pageNum);
                        }
                        break;
                    case 3:
                        break;
                }

                Console.WriteLine();
            } while (option == 1 || option == 2);
            StudentList.View(5, pageNum);

            void ViewMenu()
            {
                Console.WriteLine("**************View Student Menu**************");
                Console.WriteLine("1. Previous page");
                Console.WriteLine("2. Following page");
                Console.WriteLine("3. Back to main menu");
            }
        }
        static void FindStudent()
        {
            Console.Clear();

            bool hasValue;
                             
            Console.Write("Enter student ID or Name: ");
            string input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                if (input[0] >= 49 && input[0] <= 57)
                {
                    var findList1 = StudentList.Find(int.Parse(input), "", out hasValue);
                    if (hasValue)
                        StudentList.Display(findList1);
                    else
                        Console.WriteLine("Invalid ID");

                }
                else if ((input[0] >= 65 && input[0] <= 90) || (input[0] >= 97 && input[0] <= 122))
                {
                    var findList1 = StudentList.Find(0, input, out hasValue);
                    if (hasValue)
                        StudentList.Display(findList1);
                    else
                        Console.WriteLine("Name doesn't exist");
                }
            }
            else
            {
                Console.WriteLine("Invalid Input");
            }

            Console.WriteLine("Pres any key to continue...");
            Console.ReadKey();
        }
        static void DeleteSudent()
        {
            Console.Clear();

            int id;
            do
            {
                Console.Write("Enter Student ID: ");
                id = int.Parse(Console.ReadLine());
                if(id <= 0)
                    Console.WriteLine("Invalid ID, please enter again");
            } while (id <= 0);

            var isDeleted = StudentList.Delete(id);
            if (!isDeleted)
            {
                Console.WriteLine("Invalid student ID. Press any key to back to main menu...");
            }
            else
            {
                Console.WriteLine("Delete successfully. Press any key to back to main menu...");
            }
            Console.ReadKey();
        }
    }

    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public double Gpa { get; set; }

        public Student() { }
        public Student(int id, string name, int age, double gpa)
        {
            Id = id;
            Name = name;
            Age = age;
            Gpa = gpa;
        }
    }

    public static class StudentList
    {
        private static List<Student> student_list = new List<Student>();
        public static int Index = 0;
        public static int Count
        {
            get
            {
                return student_list.Count;
            }
        }

        public static void Display(List<Student> stuList)
        {
            foreach(var stu in stuList)
            {
                Console.WriteLine("ID: {0}, Name: {1}, Age: {2}, GPA: {3}", stu.Id, stu.Name, stu.Age, stu.Gpa);
            }
        }

        public static void Add(Student stu)
        {
            student_list.Add(stu);
        }

        public static void View(int numberOfRow, int pageNum)
        {
            var tempList = student_list.Skip((pageNum-1) * numberOfRow).Take(numberOfRow).ToList();
            foreach(var stu in tempList)
            {
                Console.WriteLine("ID: {0}, Name: {1}, Age: {2}, GPA: {3}", stu.Id, stu.Name,stu.Age,stu.Gpa);
            }
        }
        
        public static List<Student> Find(int id, string name, out bool hasValue)
        {
            List<Student> result;
            if(id != 0)
            {
                result = student_list.Where(x => x.Id == id).ToList();
            }
            else
            {
                result = student_list.Where(x => x.Name.Contains(name)).ToList();
            }

            if(result.Count == 0)
            {
                hasValue = false;
                return null;
            }

            hasValue = true;
            return result;
        }

        public static bool Delete(int id)
        {
            foreach(var stu in student_list)
            {
                if (stu.Id == id)
                {
                    student_list.RemoveAt(student_list.IndexOf(stu));
                    return true;
                }
            }

            return false;
        }
    }
}
